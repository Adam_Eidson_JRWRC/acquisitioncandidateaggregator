CREATE TABLE [dbo].[KaseGroup]
(
[LocalListingId] [int] NOT NULL IDENTITY(1, 1),
[ListingName] [varchar] (72) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullAddress] [varchar] (91) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (123) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
