CREATE TABLE [dbo].[LemRX]
(
[LocalListingId] [int] NOT NULL IDENTITY(1, 1),
[ListingName] [varchar] (19) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullAddress] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseTerms] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (122) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
