CREATE TABLE [STAGE].[MarcusMillichap]
(
[PropertyId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PropertyName] [varchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingPrice] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullAddress] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenantName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearsRemainingOnLease] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearBuilt] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PropertyUrl] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
