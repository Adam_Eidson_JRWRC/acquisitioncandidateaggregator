CREATE TABLE [STAGE].[FortisNetBrokerList]
(
[BrokerWebsiteId] [bigint] NULL,
[Name] [varchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NameWithLicense] [varchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
