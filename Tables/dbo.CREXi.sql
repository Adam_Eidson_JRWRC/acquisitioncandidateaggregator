CREATE TABLE [dbo].[CREXi]
(
[Id] [bigint] NULL,
[Url] [varchar] (46) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedOn] [datetimeoffset] (0) NULL,
[ActivatedOn] [datetimeoffset] (0) NULL,
[BrokerOfRecordName] [varchar] (115) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BrokerOfRecordLicense] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
