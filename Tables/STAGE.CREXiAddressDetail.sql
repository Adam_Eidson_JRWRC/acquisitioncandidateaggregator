CREATE TABLE [STAGE].[CREXiAddressDetail]
(
[Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street] [varchar] (58) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (52) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[County] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [varchar] (19) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Longitude] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
