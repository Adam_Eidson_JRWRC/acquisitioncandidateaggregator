CREATE TABLE [dbo].[BrokerAgent]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[FirstName] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREXiSearched] [bit] NULL
) ON [PRIMARY]
GO
