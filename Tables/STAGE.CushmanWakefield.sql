CREATE TABLE [STAGE].[CushmanWakefield]
(
[ListingId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BrokerName] [varchar] (27) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BrokerEmail] [varchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BrokerPhone] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BrokerTitle] [varchar] (87) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Longitude] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingUrl] [varchar] (350) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
