CREATE TABLE [dbo].[SullivanWickleyList]
(
[LocalListingId] [int] NOT NULL IDENTITY(1, 1),
[Title] [varchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [bigint] NULL,
[CapRate] [float] NULL,
[LeaseType] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
