CREATE TABLE [STAGE].[AvisonYoungNetLease]
(
[ListingId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Longitude] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (93) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseType] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseTermRemaining] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
