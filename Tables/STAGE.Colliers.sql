CREATE TABLE [STAGE].[Colliers]
(
[ListingId] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BrokerEmail] [varchar] (37) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Longitude] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingUrl] [varchar] (350) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
