CREATE TABLE [dbo].[YAFC]
(
[LocalListingId] [int] NOT NULL IDENTITY(1, 1),
[Title] [varchar] (74) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (28) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseTerm] [varchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (179) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
