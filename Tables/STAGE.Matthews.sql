CREATE TABLE [STAGE].[Matthews]
(
[id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[latitude] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[longitude] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[type] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[title] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[status] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[address] [varchar] (54) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[city] [varchar] (48) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[state] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[zip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[year_built] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[price] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[noi] [varchar] (33) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[leasable_area] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[price_per_sq] [float] NULL,
[cap_rate] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[units] [bigint] NULL,
[link] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[space_use] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
