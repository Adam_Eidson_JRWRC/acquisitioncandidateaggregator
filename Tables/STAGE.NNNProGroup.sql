CREATE TABLE [STAGE].[NNNProGroup]
(
[ListingId] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NOI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearBuilt] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearRenovated] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseExpiration] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseTermRemaining] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tenant] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingUrl] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
