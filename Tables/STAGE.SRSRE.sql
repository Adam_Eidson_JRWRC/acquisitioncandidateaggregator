CREATE TABLE [STAGE].[SRSRE]
(
[ListingId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CityStateZip] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (93) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SF] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
