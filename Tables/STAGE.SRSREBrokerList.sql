CREATE TABLE [STAGE].[SRSREBrokerList]
(
[ListingId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CellPhone] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OfficePhone] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobTitle] [varchar] (92) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Location] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[License] [varchar] (19) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
