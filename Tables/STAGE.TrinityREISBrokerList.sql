CREATE TABLE [STAGE].[TrinityREISBrokerList]
(
[BrokerWebsiteId] [bigint] NULL,
[Name] [varchar] (72) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (28) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NameWithLicense] [varchar] (72) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
