CREATE TABLE [STAGE].[PHNLGListingAttribute]
(
[BrokerWebsiteId] [bigint] NULL,
[Attribute] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
