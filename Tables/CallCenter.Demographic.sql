CREATE TABLE [CallCenter].[Demographic]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[BrokerWebsiteId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BrokerWebsiteSource] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Mile1TotalPopulation] [int] NULL,
[Mile1MedianHHIncome] [int] NULL,
[Mile1TotalHousingUnit] [int] NULL,
[Mile1VACANTHousingUnit] [int] NULL,
[Mile1OwnerOccupiedHousingUnit] [int] NULL,
[Mile1RentedHousingUnit] [int] NULL,
[Mile1AverageHHIncome] [int] NULL,
[Mile3TotalPopulation] [int] NULL,
[Mile3MedianHHIncome] [int] NULL,
[Mile3TotalHousingUnit] [int] NULL,
[Mile3VACANTHousingUnit] [int] NULL,
[Mile3OwnerOccupiedHousingUnit] [int] NULL,
[Mile3RentedHousingUnit] [int] NULL,
[Mile3AverageHHIncome] [int] NULL,
[Mile5TotalPopulation] [int] NULL,
[Mile5MedianHHIncome] [int] NULL,
[Mile5TotalHousingUnit] [int] NULL,
[Mile5VACANTHousingUnit] [int] NULL,
[Mile5OwnerOccupiedHousingUnit] [int] NULL,
[Mile5RentedHousingUnit] [int] NULL,
[Mile5AverageHHIncome] [int] NULL,
[CrimeIndex] [int] NULL
) ON [PRIMARY]
GO
