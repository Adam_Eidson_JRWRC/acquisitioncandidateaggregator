CREATE TABLE [STAGE].[IssenbergBrittiGroup]
(
[ListingId] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingName] [varchar] (107) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullAddress] [varchar] (91) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RemainingLeaseTerm] [varchar] (23) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (149) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
