CREATE TABLE [STAGE].[SnyderCarlton]
(
[ListingId] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingName] [varchar] (39) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street] [varchar] (58) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CityStateZip] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingUrl] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
