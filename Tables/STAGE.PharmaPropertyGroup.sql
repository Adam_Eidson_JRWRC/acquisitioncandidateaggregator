CREATE TABLE [STAGE].[PharmaPropertyGroup]
(
[ListingId] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (59) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseTermRemaining] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearBuilt] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseType] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [varchar] (350) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
