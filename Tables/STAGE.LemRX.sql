CREATE TABLE [STAGE].[LemRX]
(
[ListingName] [varchar] (19) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullAddress] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseTerms] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (122) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocalListingId] [int] NULL
) ON [PRIMARY]
GO
