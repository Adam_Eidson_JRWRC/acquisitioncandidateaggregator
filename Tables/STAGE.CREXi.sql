CREATE TABLE [STAGE].[CREXi]
(
[Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Url] [varchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedOn] [varchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActivatedOn] [varchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BrokerOfRecordName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BrokerOfRecordLicense] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
