CREATE TABLE [STAGE].[SandsIG-test]
(
[Column 0] [bigint] NULL,
[Column 1] [varchar] (73) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 2] [varchar] (63) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 3] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 4] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 5] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 6] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 7] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column 8] [varchar] (146) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
