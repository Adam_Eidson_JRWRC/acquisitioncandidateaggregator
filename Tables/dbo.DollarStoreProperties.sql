CREATE TABLE [dbo].[DollarStoreProperties]
(
[LocalListingId] [int] NOT NULL IDENTITY(1090000, 1),
[ListingName] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
