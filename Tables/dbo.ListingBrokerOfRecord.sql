CREATE TABLE [dbo].[ListingBrokerOfRecord]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[ListingId] [int] NOT NULL,
[Name] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[License] [varchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phone] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_ListingBrokerOfRecord_18_1877581727__K2_K3] ON [dbo].[ListingBrokerOfRecord] ([ListingId], [Name]) ON [PRIMARY]
GO
