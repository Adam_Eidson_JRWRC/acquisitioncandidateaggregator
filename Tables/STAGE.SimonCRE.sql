CREATE TABLE [STAGE].[SimonCRE]
(
[ListingId] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Longitude] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
