CREATE TABLE [STAGE].[MidAmericaRealEstateCorp]
(
[BrokerWebsiteId] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Size] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingName] [varchar] (102) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (33) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubType] [varchar] (72) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Longitude] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (243) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearBuilt] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
