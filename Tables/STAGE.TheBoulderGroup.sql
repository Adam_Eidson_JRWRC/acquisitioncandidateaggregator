CREATE TABLE [STAGE].[TheBoulderGroup]
(
[idProperty] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[idActive] [bigint] NULL,
[timestamp] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[idPriority] [bigint] NULL,
[title] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[city] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[state] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[price] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[cap] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[capLabel] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[term] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[termLabel] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[pdf] [varchar] (56) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[image] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [decimal] (12, 9) NULL,
[Longitude] [decimal] (12, 9) NULL
) ON [PRIMARY]
GO
