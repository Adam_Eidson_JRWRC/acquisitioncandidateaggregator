CREATE TABLE [dbo].[BrokerAgentLicense]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[BrokerAgentId] [int] NOT NULL,
[License] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
