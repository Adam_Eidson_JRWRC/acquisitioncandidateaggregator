CREATE TABLE [STAGE].[LeeAndAssociatesBrokerList]
(
[BrokerWebsiteId] [bigint] NULL,
[Name] [varchar] (66) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NameWithLicense] [varchar] (66) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [varchar] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
