CREATE TABLE [STAGE].[WellsNLG]
(
[ListingName] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (37) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseTermRemaining] [varchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (118) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocalListingId] [int] NULL
) ON [PRIMARY]
GO
