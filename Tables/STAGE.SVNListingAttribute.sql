CREATE TABLE [STAGE].[SVNListingAttribute]
(
[BrokerWebsiteId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Attribute] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
