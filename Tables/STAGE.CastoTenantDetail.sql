CREATE TABLE [STAGE].[CastoTenantDetail]
(
[ListingTitle] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenantName] [varchar] (175) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocalListingId] [int] NULL
) ON [PRIMARY]
GO
