CREATE TABLE [STAGE].[NetLeaseAdvisoryGroup]
(
[ListingName] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NOI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Longitude] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocalListingId] [int] NULL
) ON [PRIMARY]
GO
