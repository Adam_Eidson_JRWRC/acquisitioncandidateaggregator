CREATE TABLE [STAGE].[CREXiBrokerLicense]
(
[BrokerId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[License] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
