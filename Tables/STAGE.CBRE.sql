CREATE TABLE [STAGE].[CBRE]
(
[Title] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AssetTypeFull] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AssetType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AssetTypeSubType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingNOI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingOccupancy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingParcelType] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingParcelSize] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingSize] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingSizeType] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingPrice] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingYearBuilt] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (140) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
