CREATE TABLE [STAGE].[LeeAndAssociates]
(
[BrokerWebsiteId] [bigint] NULL,
[Size] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingName] [varchar] (102) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street] [varchar] (67) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (33) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubType] [varchar] (72) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [float] NULL,
[Longitude] [float] NULL,
[ListingURL] [varchar] (243) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
