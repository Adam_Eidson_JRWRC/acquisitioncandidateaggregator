CREATE TABLE [dbo].[TheBoulderGroup]
(
[idProperty] [bigint] NULL,
[idActive] [bigint] NULL,
[timestamp] [datetime] NULL,
[idPriority] [bigint] NULL,
[title] [varchar] (39) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[city] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[state] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[price] [bigint] NULL,
[cap] [float] NULL,
[capLabel] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[term] [bigint] NULL,
[termLabel] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[pdf] [varchar] (56) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[image] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
