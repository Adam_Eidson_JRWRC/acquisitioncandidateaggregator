CREATE TABLE [STAGE].[FriedmanRealEstate]
(
[BrokerWebsiteId] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Size] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubType] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [varchar] (26) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Longitude] [varchar] (26) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
