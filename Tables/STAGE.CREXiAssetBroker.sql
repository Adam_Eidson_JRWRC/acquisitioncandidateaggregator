CREATE TABLE [STAGE].[CREXiAssetBroker]
(
[BrokerId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ListingId] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (72) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumberOfAssets] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerBrokerage] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
