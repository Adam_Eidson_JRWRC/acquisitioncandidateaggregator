CREATE TABLE [STAGE].[Casto]
(
[ListingName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Longitude] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (122) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocalListingId] [int] NULL
) ON [PRIMARY]
GO
