CREATE TABLE [STAGE].[KaseGroup]
(
[ListingName] [varchar] (72) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullAddress] [varchar] (91) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (123) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocalListingId] [int] NULL
) ON [PRIMARY]
GO
