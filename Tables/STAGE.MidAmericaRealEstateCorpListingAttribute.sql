CREATE TABLE [STAGE].[MidAmericaRealEstateCorpListingAttribute]
(
[BrokerWebsiteId] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Attribute] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
