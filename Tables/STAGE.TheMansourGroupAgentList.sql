CREATE TABLE [STAGE].[TheMansourGroupAgentList]
(
[ListingId] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AgentName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AgentEmail] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AgentPhone] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
