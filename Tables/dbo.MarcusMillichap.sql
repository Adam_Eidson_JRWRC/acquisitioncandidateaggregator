CREATE TABLE [dbo].[MarcusMillichap]
(
[PropertyId] [bigint] NULL,
[PropertyName] [varchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PropertyTypeId] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PropertyType] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PropertySubTypeId] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PropertySubType] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstImageUrl] [varchar] (117) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SecondImageUrl] [varchar] (117) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [varchar] (68) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateProvince] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateProvinceName] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DynamicDataText] [varchar] (28) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DynamicDataValue] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [float] NULL,
[ListingPrice] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PropertyUrl] [varchar] (166) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tile] [varchar] (1267) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
