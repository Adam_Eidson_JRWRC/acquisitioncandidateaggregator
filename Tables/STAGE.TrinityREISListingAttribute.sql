CREATE TABLE [STAGE].[TrinityREISListingAttribute]
(
[BrokerWebsiteId] [bigint] NULL,
[Attribute] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
