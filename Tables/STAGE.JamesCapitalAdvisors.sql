CREATE TABLE [STAGE].[JamesCapitalAdvisors]
(
[ListingId] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RemainingTerm] [varchar] (22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseType] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearBuilt] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (23) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Longitude] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (101) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
