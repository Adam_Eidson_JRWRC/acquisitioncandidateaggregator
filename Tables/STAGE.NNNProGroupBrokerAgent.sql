CREATE TABLE [STAGE].[NNNProGroupBrokerAgent]
(
[ListingId] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AgentName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AgentEmail] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AgentPhone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
