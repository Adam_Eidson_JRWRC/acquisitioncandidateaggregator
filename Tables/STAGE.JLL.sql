CREATE TABLE [STAGE].[JLL]
(
[ListingId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (93) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseTermRemaining] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [varchar] (82) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Longitude] [varchar] (82) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
