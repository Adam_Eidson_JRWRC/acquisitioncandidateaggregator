CREATE TABLE [STAGE].[NNNInvestmentAdvisors]
(
[ListingId] [varchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [varchar] (84) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseType] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseExpiration] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (140) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearBuilt] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
