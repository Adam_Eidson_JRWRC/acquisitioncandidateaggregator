CREATE TABLE [STAGE].[FortisNet]
(
[BrokerWebsiteId] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Size] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingName] [varchar] (62) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street] [varchar] (51) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubType] [varchar] (26) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [float] NULL,
[Longitude] [float] NULL,
[ListingURL] [varchar] (133) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
