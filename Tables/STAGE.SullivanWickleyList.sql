CREATE TABLE [STAGE].[SullivanWickleyList]
(
[Title] [varchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseType] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocalListingId] [int] NULL
) ON [PRIMARY]
GO
