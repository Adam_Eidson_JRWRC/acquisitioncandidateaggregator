CREATE TABLE [dbo].[ListingTenant]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[ListingId] [int] NOT NULL,
[TenantName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ImprovedTenantCapture] [bit] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ListingTenant_ID_NCIDX] ON [dbo].[ListingTenant] ([ListingId]) INCLUDE ([id]) ON [PRIMARY]
GO
