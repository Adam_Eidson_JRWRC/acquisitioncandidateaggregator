CREATE TABLE [STAGE].[SandsIG]
(
[BrokerWebsiteId] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
