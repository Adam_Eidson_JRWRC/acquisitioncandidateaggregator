CREATE TABLE [STAGE].[SambazisRetailGroup]
(
[ListingId] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingName] [varchar] (91) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NOI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearBuilt] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingUrl] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
