CREATE TABLE [STAGE].[LeeAndAssociatesListingAttribute]
(
[BrokerWebsiteId] [bigint] NULL,
[Attribute] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [varchar] (72) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
