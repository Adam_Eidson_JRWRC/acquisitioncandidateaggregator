CREATE TABLE [STAGE].[TrinityREIS]
(
[BrokerWebsiteId] [bigint] NULL,
[Size] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingName] [varchar] (85) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street] [varchar] (61) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (33) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubType] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [float] NULL,
[Longitude] [float] NULL,
[ListingURL] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
