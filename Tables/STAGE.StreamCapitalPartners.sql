CREATE TABLE [STAGE].[StreamCapitalPartners]
(
[ListingId] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseType] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseExpiration] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SqFt] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
