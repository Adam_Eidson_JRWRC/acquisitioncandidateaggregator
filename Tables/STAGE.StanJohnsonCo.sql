CREATE TABLE [STAGE].[StanJohnsonCo]
(
[listing_name] [varchar] (81) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cap_RateEquity] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[details] [varchar] (121) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ViewDetails] [varchar] (240) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocalListingId] [int] NULL
) ON [PRIMARY]
GO
