CREATE TABLE [STAGE].[Schuchert]
(
[Title] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (93) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street] [varchar] (33) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (19) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseTerm] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearBuilt] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseExpiration] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingNOI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocalListingId] [int] NULL
) ON [PRIMARY]
GO
