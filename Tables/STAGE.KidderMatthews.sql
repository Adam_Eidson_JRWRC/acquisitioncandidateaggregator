CREATE TABLE [STAGE].[KidderMatthews]
(
[BrokerWebsiteId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Size] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseExpirationDate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingName] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street] [varchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Longitude] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (88) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearRemodeled] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearBuilt] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
