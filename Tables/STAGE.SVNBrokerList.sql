CREATE TABLE [STAGE].[SVNBrokerList]
(
[BrokerWebsiteId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (28) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NameWithLicense] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [varchar] (28) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
