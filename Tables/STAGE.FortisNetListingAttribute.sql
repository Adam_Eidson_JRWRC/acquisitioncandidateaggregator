CREATE TABLE [STAGE].[FortisNetListingAttribute]
(
[BrokerWebsiteId] [bigint] NULL,
[Attribute] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
