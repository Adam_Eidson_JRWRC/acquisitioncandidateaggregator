CREATE TABLE [STAGE].[TheMansourGroup]
(
[ListingId] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street] [varchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (48) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Longitude] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapRate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseTerm] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearBuilt] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenantMap] [varchar] (52) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (98) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
