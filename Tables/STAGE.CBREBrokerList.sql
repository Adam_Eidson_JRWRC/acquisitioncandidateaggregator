CREATE TABLE [STAGE].[CBREBrokerList]
(
[ListingId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Company] [varchar] (48) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (38) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[License] [varchar] (115) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (55) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
