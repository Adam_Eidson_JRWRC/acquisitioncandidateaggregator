SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 11/13/2020
-- Description:	Stored Procedure to populate BrokerWebsiteId for Casto, which does not supply a broker id from their website. 
-- This is used to keep track of what records have been inserted to SF, and which have not.
-- =============================================
CREATE PROCEDURE [dbo].[PopulateCastoBrokerWebsiteId]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

  --Populate Local Datastore Id where no website match exists
  Insert Into AcquisitionCandidateAggregator.dbo.Casto
  (
		[ListingName] ,
	[Latitude],
	[Longitude] ,
	[ListingURL]
  )

  select stg.[ListingName]
      ,stg.[Latitude]
      ,stg.[Longitude]
	  ,stg.[ListingURL]
  from AcquisitionCandidateAggregator.STAGE.Casto stg
  left join  AcquisitionCandidateAggregator.dbo.Casto loc on stg.[ListingName] = loc.[ListingName]
  and stg.[Latitude] = loc.[Latitude]
  and stg.[Longitude] = loc.[Longitude]
  where loc.ListingURL is null

  --update stage table so other processes do not need to be updated to accommodate the local datastore. --AE
  update stg
  set  LocalListingId = loc.LocalListingId
  from AcquisitionCandidateAggregator.STAGE.Casto stg
  left join  AcquisitionCandidateAggregator.dbo.Casto loc on stg.[ListingName] = loc.[ListingName]
  and stg.[Latitude] = loc.[Latitude]
  and stg.[Longitude] = loc.[Longitude]
  where loc.LocalListingId is not null
  and stg.LocalListingId is null

  --update Casto Broker Agent stage table
  update stg
  set  LocalListingId = loc.LocalListingId
  from AcquisitionCandidateAggregator.STAGE.CastoBrokerList stg
  left join  AcquisitionCandidateAggregator.dbo.Casto loc on stg.[ListingTitle] = loc.[ListingName]
  where loc.LocalListingId is not null
  and stg.LocalListingId is null

  --update Casto Tenant stage table
  update stg
  set  LocalListingId = loc.LocalListingId
  from AcquisitionCandidateAggregator.STAGE.CastoTenantDetail stg
  left join  AcquisitionCandidateAggregator.dbo.Casto loc on stg.[ListingTitle] = loc.[ListingName]
  where loc.LocalListingId is not null
  and stg.LocalListingId is null
END
GO
