SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 11/13/2020
-- Description:	Stored Procedure to populate BrokerWebsiteId for YAFC, which does not supply a broker id from their website. 
-- This is used to keep track of what records have been inserted to SF, and which have not.
-- =============================================
CREATE PROCEDURE [dbo].[PopulateYAFCBrokerWebsiteId]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

  --Populate Local Datastore Id where no website match exists
  Insert Into AcquisitionCandidateAggregator.dbo.YAFC
  (
		[Title]
      ,[City]
      ,[State]
      ,[Price]
      ,[CapRate]
      ,[LeaseTerm]
      ,[ListingURL]
  )

  select stg.[Title]
      ,stg.[City]
      ,stg.[State]
      ,stg.[Price]
      ,stg.[CapRate]
      ,stg.[LeaseTerm]
      ,stg.[ListingURL]
  from AcquisitionCandidateAggregator.STAGE.YAFC stg
  left join  AcquisitionCandidateAggregator.dbo.YAFC loc on stg.[Title] = loc.[Title]
  and stg.[City] = loc.[City]
  and stg.[LeaseTerm] = loc.[LeaseTerm]
  and stg.Price = loc.Price
  where loc.ListingURL is null

  --update stage table so other processes do not need to be updated to accommodate the local datastore. --AE
  update stg
  set  LocalListingId = loc.LocalListingId
  from AcquisitionCandidateAggregator.STAGE.YAFC stg
  left join  AcquisitionCandidateAggregator.dbo.YAFC loc on stg.[Title] = loc.[Title]
  and stg.[City] = loc.[City]
  and stg.[LeaseTerm] = loc.[LeaseTerm]
  and stg.Price = loc.Price
  where loc.LocalListingId is not null
  and stg.LocalListingId is null
END




GO
GRANT EXECUTE ON  [dbo].[PopulateYAFCBrokerWebsiteId] TO [JRW\etl_user]
GO
