SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 3/19/2021
-- Description:	Identifies broker agents associated with approved tenant listings, parses out first name, last name, email, and phone number if  possible, and inserts into BrokerAgent.
--				This should be executed after the SSIS operation and before the Property data push to SF.
-- =============================================
CREATE PROCEDURE [dbo].[InsertQualifiedBrokerAgent]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

;with cte_prep as (
SELECT distinct 
case when TRIM(substring(REPLACE(BrokerOfRecordName, 'Broker of Record: ',''), 0,charindex(',',REPLACE(BrokerOfRecordName, 'Broker of Record: ',''), 0))) != ''
and TRIM(substring(REPLACE(BrokerOfRecordName, 'Broker of Record: ',''), 0,charindex('-',REPLACE(BrokerOfRecordName, 'Broker of Record: ',''), 0))) = ''
then TRIM(substring(REPLACE(BrokerOfRecordName, 'Broker of Record: ',''), 0,charindex(',',REPLACE(BrokerOfRecordName, 'Broker of Record: ',''), 0)))
when TRIM(substring(REPLACE(BrokerOfRecordName, 'Broker of Record: ',''), 0,charindex(',',REPLACE(BrokerOfRecordName, 'Broker of Record: ',''), 0))) = ''
and TRIM(substring(REPLACE(BrokerOfRecordName, 'Broker of Record: ',''), 0,charindex('-',REPLACE(BrokerOfRecordName, 'Broker of Record: ',''), 0))) != ''
then TRIM(substring(REPLACE(BrokerOfRecordName, 'Broker of Record: ',''), 0,charindex('-',REPLACE(BrokerOfRecordName, 'Broker of Record: ',''), 0))) 
when TRIM(substring(REPLACE(BrokerOfRecordName, 'Broker of Record: ',''), 0,charindex(',',REPLACE(BrokerOfRecordName, 'Broker of Record: ',''), 0))) != ''
and TRIM(substring(REPLACE(BrokerOfRecordName, 'Broker of Record: ',''), 0,charindex('-',REPLACE(BrokerOfRecordName, 'Broker of Record: ',''), 0))) != ''
then TRIM(substring(substring(REPLACE(BrokerOfRecordName, 'Broker of Record: ',''), 0,charindex('-',REPLACE(BrokerOfRecordName, 'Broker of Record: ',''), 0)),0,charindex(',',REPLACE(BrokerOfRecordName, 'Broker of Record: ',''), 0))) 
when substring(BrokerOfRecordName,0, charindex('Broker of Record: ', BrokerOfRecordName,0)) != ''
then substring(BrokerOfRecordName,0, charindex('Broker of Record', BrokerOfRecordName,0)) 
else BrokerOfRecordName
end as ParsedName
, lbor.BrokerOfRecordEmail,
lbor.BrokerOfRecordPhone
  FROM AcquisitionCandidateAggregator.dbo.vBrokerWebsiteDataApprovedTenant lbor--listingbrokerofrecord lbor
  ), cte_parsedName as (
  select distinct TRIM(substring(parsedName, 0,charindex(' ',parsedName,0))) as FirstName,
  TRIM(substring(parsedName,charindex(' ',parsedName,0),50)) as LastName--,
  From cte_prep lbor
  where ParsedName is not null
  and (ParsedName not like '%Realty%'
  and ParsedName not like '%Associate%'
  and ParsedName not like '%Partner%'
  and ParsedName not like '%Group%'
  and ParsedName not like '%Estate%'
   )
  )


  insert into [AcquisitionCandidateAggregator].[dbo].[BrokerAgent]
  (
  [FirstName]
      ,[LastName]
      ,[email]
      ,[phone]
	  ,CREXiSearched
  )

 
  SELECT distinct cpn.firstName,
  cpn.lastName,
  lbor.email,
  lbor.phone,
  0 as CREXiSearched
  FROM cte_parsedName cpn
  join AcquisitionCandidateAggregator.dbo.listingbrokerofrecord lbor on lbor.name like cpn.FirstName+' '+cpn.LastName
  left join AcquisitionCandidateAggregator.dbo.BrokerAgent ba on ba.FirstName+' '+ba.LastName = cpn.FirstName+' '+cpn.LastName
  where ba.id is null

END
GO
GRANT EXECUTE ON  [dbo].[InsertQualifiedBrokerAgent] TO [JRW\etl_user]
GO
