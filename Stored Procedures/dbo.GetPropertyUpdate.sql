SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Adam Eidson
-- Create date: 12/4/2020
-- Description:	Returns local db property listings that have been updated in the given time frame.
-- =============================================
--exec AcquisitionCandidateAggregator.[dbo].[GetPropertyUpdate]
CREATE PROCEDURE [dbo].[GetPropertyUpdate] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   
DECLARE @from_lsn binary (10) ,@to_lsn binary (10)
DECLARE @NamePosition INT
DECLARE @ListingURLPosition INT
DECLARE @ContractPricePosition INT
DECLARE @NOIPosition INT
DECLARE @SqFtPosition INT
DECLARE @YearBuiltPosition INT
DECLARE @StreetPosition INT
DECLARE @CityPosition INT
DECLARE @StatePosition  INT
DECLARE @ZipPosition  INT
DECLARE @yearRenovatedPosition  INT
DECLARE @CurrentTermExpirationDatePosition  INT
DECLARE @ActivePosition  INT

SET @from_lsn = sys.fn_cdc_map_time_to_lsn('smallest greater than',DATEADD(hour,-4,GETDATE()))--sys.fn_cdc_get_min_lsn('dbo_Individual')
SET @to_lsn = sys.fn_cdc_get_max_lsn()
SET @NamePosition = sys.fn_cdc_get_column_ordinal('dbo_Listing', 
'Name')
SET @ListingURLPosition = sys.fn_cdc_get_column_ordinal('dbo_Listing', 
'ListingURL')
SET @ContractPricePosition = sys.fn_cdc_get_column_ordinal('dbo_Listing', 
'ContractPrice')
SET @NOIPosition = sys.fn_cdc_get_column_ordinal('dbo_Listing', 
'NOI')
SET @SqFtPosition = sys.fn_cdc_get_column_ordinal('dbo_Listing', 'SqFt')
SET @YearBuiltPosition = sys.fn_cdc_get_column_ordinal('dbo_Listing', 'YearBuilt')
SET @StreetPosition = sys.fn_cdc_get_column_ordinal('dbo_Listing', 'Street')
SET @CityPosition = sys.fn_cdc_get_column_ordinal('dbo_Listing', 'City')
SET @StatePosition = sys.fn_cdc_get_column_ordinal('dbo_Listing', 'State')
SET @ZipPosition = sys.fn_cdc_get_column_ordinal('dbo_Listing', 'Zip')
SET @yearRenovatedPosition = sys.fn_cdc_get_column_ordinal('dbo_Listing', 'yearRenovated')
SET @CurrentTermExpirationDatePosition = sys.fn_cdc_get_column_ordinal('dbo_Listing', 'CurrentTermExpirationDate')
SET @ActivePosition = sys.fn_cdc_get_column_ordinal('dbo_Listing', 'Active')

SELECT fn_cdc_get_all_changes_dbo_Listing.__$start_lsn
	,fn_cdc_get_all_changes_dbo_Listing.Id
	,fn_cdc_get_all_changes_dbo_Listing.__$operation
	,fn_cdc_get_all_changes_dbo_Listing.__$update_mask
	,sys.fn_cdc_is_bit_set(@NamePosition, __$update_mask) as 
'Name'
	,sys.fn_cdc_is_bit_set(@ListingURLPosition, __$update_mask) as 
'ListingURL'
	,sys.fn_cdc_is_bit_set(@ContractPricePosition, __$update_mask) as 
'ContractPrice'
,sys.fn_cdc_is_bit_set(@NOIPosition, __$update_mask) as 
'NOI'
	,sys.fn_cdc_is_bit_set(@SqFtPosition , __$update_mask) as 
'SqFt'	
,sys.fn_cdc_is_bit_set(@YearBuiltPosition , __$update_mask) as 
'YearBuilt'	
,sys.fn_cdc_is_bit_set(@StreetPosition , __$update_mask) as 
'Street'	
,sys.fn_cdc_is_bit_set(@CityPosition , __$update_mask) as 
'City'	
,sys.fn_cdc_is_bit_set(@StatePosition , __$update_mask) as 
'State'	
,sys.fn_cdc_is_bit_set(@ZipPosition , __$update_mask) as 
'Zip'	
,sys.fn_cdc_is_bit_set(@yearRenovatedPosition , __$update_mask) as 
'yearRenovated'	
,sys.fn_cdc_is_bit_set(@CurrentTermExpirationDatePosition , __$update_mask) as 
'CurrentTermExpirationDate'	
,sys.fn_cdc_is_bit_set(@ActivePosition , __$update_mask) as 
'Active'	
into #tempPropertyChangeBitMap
FROM cdc.fn_cdc_get_all_changes_dbo_Listing(@from_lsn, @to_lsn, 'all')
where __$operation = 4 --only grab updates. We are taking care of inserts another way.
and 
/* Only return records that had fields that we care about updated. May expand this in the future.*/
(sys.fn_cdc_is_bit_set(@NamePosition, __$update_mask) = 1 OR
	sys.fn_cdc_is_bit_set(@ListingURLPosition, __$update_mask) = 1 OR
	sys.fn_cdc_is_bit_set(@ContractPricePosition, __$update_mask) = 1 OR
	sys.fn_cdc_is_bit_set(@NOIPosition, __$update_mask) = 1 OR
	sys.fn_cdc_is_bit_set(@SqFtPosition , __$update_mask) = 1 OR
sys.fn_cdc_is_bit_set(@YearBuiltPosition , __$update_mask) = 1 OR
sys.fn_cdc_is_bit_set(@StreetPosition , __$update_mask) = 1 OR
sys.fn_cdc_is_bit_set(@CityPosition , __$update_mask) = 1 OR
sys.fn_cdc_is_bit_set(@StatePosition , __$update_mask) = 1 OR
sys.fn_cdc_is_bit_set(@ZipPosition , __$update_mask) = 1 OR
sys.fn_cdc_is_bit_set(@yearRenovatedPosition , __$update_mask) = 1 OR
sys.fn_cdc_is_bit_set(@CurrentTermExpirationDatePosition , __$update_mask) = 1 OR
sys.fn_cdc_is_bit_set(@ActivePosition , __$update_mask) = 1
)
ORDER BY __$seqval

select 
prep.id,
[Name],
ListingURL as Listing_URL__c,
ContractPrice as Contract_Price__c,
NOI as Purchase_NOI_Override__c,
NOI as Purchase_NOI__c,
SqFt as Sq_Ft__c,
YearBuilt as Year_Built__c,
Street as Street__c,
City as City__c,
State as State__c,
Zip as Zip__c,
YearRenovated as Year_Renovated__c,
CurrentTermExpirationDate as Current_Term_Expiration_Date__c,
case when Active=1 and stage__c = 'Listing-Expired'
then 'Listing-Active'
when Active=1 and stage__c = 'Listing-Exired'
then 'Listing-Active'
when Active=0 and stage__c = 'Listing-Active'
then 'Listing-Expired'
end as Stage__c
from (
select distinct SF_Property.id,
case when t.[Name] = 1 and L.[Name] is not null
then L.[Name]
end as Name,
case when t.ListingURL = 1
then L.ListingURL
end as ListingURL,
case when t.ContractPrice = 1
then L.ContractPrice
end as ContractPrice,
case when t.NOI = 1
then L.NOI
end as NOI,
case when t.SqFt = 1
then L.SqFt
end as SqFt,
case when t.YearBuilt = 1
then L.YearBuilt
end as YearBuilt,
case when t.Street = 1
then L.Street
end as Street,
case when t.City = 1
then L.City
end as City,
case when t.State = 1
then L.State
end as State,
case when t.Zip = 1
then L.Zip
end as Zip,
case when t.YearRenovated = 1
then L.YearRenovated
end as YearRenovated,
case when t.CurrentTermExpirationDate = 1
then L.CurrentTermExpirationDate
end as CurrentTermExpirationDate,
case when t.Active = 1
then L.Active
end as Active,
SF_Property.Stage__c
From #tempPropertyChangeBitMap t 
join AcquisitionCandidateAggregator.dbo.Listing L on L.id = t.id
join SalesForce_reporting.dbo.vProperty SF_Property on SF_Property.Listing_Website_Id__c = L.ListingWebsiteId
--and SF_Property.Purchase_Price__c=L.ContractPrice
where SF_Property.stage__c  in ('Listing-Active', 'Listing-Expired', 'Listing-Exired', 'Rejected') ) prep
For XML Raw ('Property'), ROOT ('Properties')

END


GO
GRANT EXECUTE ON  [dbo].[GetPropertyUpdate] TO [JRW\etl_user]
GO
