SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 3/29/2021
-- Description:	Stored Procedure to populate BrokerWebsiteId for Illuminate Real Estate, which does not supply a broker id from their website. 
-- This is used to keep track of what records have been inserted to SF, and which have not.
-- =============================================
--exec AcquisitionCandidateAggregator.[dbo].[[PopulateIlluminateRealEstateBrokerWebsiteId]]
CREATE PROCEDURE [dbo].[PopulateIlluminateRealEstateBrokerWebsiteId]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

  --Populate Local Datastore Id where no website match exists
  Insert Into AcquisitionCandidateAggregator.dbo.IlluminateRealEstate
  ([ListingName] ,
	Street,
	CityStateZip,
	[Price] ,
	[CapRate] ,
	[NOI] ,
	[ListingURL]
  )

  select stg.[ListingName] ,
	stg.Street,
	stg.CityStateZip,
	stg.[Price] ,
	stg.[CapRate] ,
	stg.NOI,
	stg.[ListingURL]
  from AcquisitionCandidateAggregator.STAGE.IlluminateRealEstate stg
  left join  AcquisitionCandidateAggregator.dbo.IlluminateRealEstate loc on stg.[ListingName] = loc.[ListingName]
  and stg.Street = loc.Street
  and ISNULL(stg.Price,'') = ISNULL(loc.Price,'')
  where loc.ListingURL is null

  --update stage table so other processes do not need to be updated to accommodate the local datastore. --AE
  update stg
  set  LocalListingId = loc.LocalListingId
  from AcquisitionCandidateAggregator.STAGE.IlluminateRealEstate stg
  left join  AcquisitionCandidateAggregator.dbo.IlluminateRealEstate loc on stg.[ListingName] = loc.[ListingName]
  and stg.Street = loc.Street
  and ISNULL(stg.Price,'') = ISNULL(loc.Price,'')
  where loc.LocalListingId is not null
  and stg.LocalListingId is null
END
GO
GRANT EXECUTE ON  [dbo].[PopulateIlluminateRealEstateBrokerWebsiteId] TO [JRW\etl_user]
GO
