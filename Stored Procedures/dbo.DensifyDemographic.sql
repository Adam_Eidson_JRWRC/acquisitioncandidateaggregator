SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 2/22/2021
-- Description:	Stored procedure to densify the demographic data on the Listing table. 
-- This could probably be eliminated if a refactor of the ETL pipeline occurs.
-- =============================================
--exec AcquisitionCandidateAggregator.dbo.DensifyDemographic
CREATE PROCEDURE [dbo].[DensifyDemographic]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   update l
set [Mile5PctOfIncomeForMortgage]=d.Mile5PctOfIncomeForMortgage
      ,[Mile5GrowthRateOwnerOccHUs]=d.[Mile5GrowthRateOwnerOccHUs]
      ,[Mile5CivPop16PlusLaborForce]=d.[Mile5CivPop16PlusLaborForce]
      ,[Mile5EmployedCivilianPop16Plus]=d.Mile5EmployedCivilianPop16Plus
      ,[Mile5UnemployedPopulation16Plus]=d.Mile5UnemployedPopulation16Plus
      ,[Mile5EmployedCivilianPop16To24]=d.Mile5EmployedCivilianPop16To24
      ,[Mile5UnemployedPop16To24]=d.Mile5UnemployedPop16To24
      ,[Mile5UnempRatePop16To24]=d.Mile5UnempRatePop16To24
      ,[Mile5EmployedCivilianPop25To54]=d.Mile5EmployedCivilianPop25To54
      ,[Mile5UnemployedPop25To54]=d.[Mile5UnemployedPop25To54]
      ,[Mile5EmployedCivilianPop55To64]=d.[Mile5EmployedCivilianPop55To64]
      ,[Mile5UnemployedPop55To64]=d.[Mile5UnemployedPop55To64]
      ,[Mile5EmployedCivilianPop65Plus]=d.[Mile5EmployedCivilianPop65Plus]
      ,[Mile5UnemployedPop65Plus]=d.[Mile5UnemployedPop65Plus]
      ,[Mile5Emp16PlusByIndustryBase]=d.[Mile5Emp16PlusByIndustryBase]
      ,[Mile5IndustryAgriculture]=d.[Mile5IndustryAgriculture]
      ,[Mile5IndustryConstruction]=d.[Mile5IndustryConstruction]
      ,[Mile5IndustryManufacturing]=d.[Mile5IndustryManufacturing]
      ,[Mile5IndustryWholesaleTrade]=d.[Mile5IndustryWholesaleTrade]
      ,[Mile5IndustryRetailTrade]=d.[Mile5IndustryRetailTrade]
      ,[Mile5IndustryTransportation]=d.[Mile5IndustryTransportation]
      ,[Mile5IndustryUtilities]=d.[Mile5IndustryUtilities]
      ,[Mile5IndustryInformation]=d.[Mile5IndustryInformation]
      ,[Mile5IndustryFinanceAndInsurance]=d.[Mile5IndustryFinanceAndInsurance]
      ,[Mile5IndustryRealEstate]=d.[Mile5IndustryRealEstate]
      ,[Mile5IndustryProfessionalAndTechSvcs]=d.[Mile5IndustryProfessionalAndTechSvcs]
      ,[Mile5IndustryAdminAndWasteMgmt]=d.[Mile5IndustryAdminAndWasteMgmt]
      ,[Mile5IndustryHealthCare]=d.[Mile5IndustryHealthCare]
      ,[Mile5IndustryAccommodationAndFoodSvcs]=d.[Mile5IndustryAccommodationAndFoodSvcs]
      ,[Mile5IndustryOtherServices]=d.[Mile5IndustryOtherServices]
      ,[Mile5IndustryPublicAdministration]=d.[Mile5IndustryPublicAdministration]
      ,[Mile3PctOfIncomeForMortgage]=d.[Mile3PctOfIncomeForMortgage]
      ,[Mile3GrowthRateOwnerOccHUs]=d.[Mile3GrowthRateOwnerOccHUs]
      ,[Mile3CivPop16PlusLaborForce]=d.[Mile3CivPop16PlusLaborForce]
      ,[Mile3EmployedCivilianPop16Plus]=d.[Mile3EmployedCivilianPop16Plus]
      ,[Mile3UnemployedPopulation16Plus]=d.[Mile3UnemployedPopulation16Plus]
      ,[Mile3EmployedCivilianPop16To24]=d.[Mile3EmployedCivilianPop16To24]
      ,[Mile3UnemployedPop16To24]=d.[Mile3UnemployedPop16To24]
      ,[Mile3UnempRatePop16To24]=d.[Mile3UnempRatePop16To24]
      ,[Mile3EmployedCivilianPop25To54]=d.[Mile3EmployedCivilianPop25To54]
      ,[Mile3UnemployedPop25To54]=d.[Mile3UnemployedPop25To54]
      ,[Mile3EmployedCivilianPop55To64]=d.[Mile3EmployedCivilianPop55To64]
      ,[Mile3UnemployedPop55To64]=d.[Mile3UnemployedPop55To64]
      ,[Mile3EmployedCivilianPop65Plus]=d.[Mile3EmployedCivilianPop65Plus]
      ,[Mile3UnemployedPop65Plus]=d.[Mile3UnemployedPop65Plus]
      ,[Mile3Emp16PlusByIndustryBase]=d.[Mile3Emp16PlusByIndustryBase]
      ,[Mile3IndustryAgriculture]=d.[Mile3IndustryAgriculture]
      ,[Mile3IndustryConstruction]=d.[Mile3IndustryConstruction]
      ,[Mile3IndustryManufacturing]=d.[Mile3IndustryManufacturing]
      ,[Mile3IndustryWholesaleTrade]=d.[Mile3IndustryWholesaleTrade]
      ,[Mile3IndustryRetailTrade]=d.[Mile3IndustryRetailTrade]
      ,[Mile3IndustryTransportation]=d.[Mile3IndustryTransportation]
      ,[Mile3IndustryUtilities]=d.[Mile3IndustryUtilities]
      ,[Mile3IndustryInformation]=d.[Mile3IndustryInformation]
      ,[Mile3IndustryFinanceAndInsurance]=d.[Mile3IndustryFinanceAndInsurance]
      ,[Mile3IndustryRealEstate]=d.[Mile3IndustryRealEstate]
      ,[Mile3IndustryProfessionalAndTechSvcs]=d.[Mile3IndustryProfessionalAndTechSvcs]
      ,[Mile3IndustryAdminAndWasteMgmt]=d.[Mile3IndustryAdminAndWasteMgmt]
      ,[Mile3IndustryHealthCare]=d.[Mile3IndustryHealthCare]
      ,[Mile3IndustryAccommodationAndFoodSvcs]=d.[Mile3IndustryAccommodationAndFoodSvcs]
      ,[Mile3IndustryOtherServices]=d.[Mile3IndustryOtherServices]
      ,[Mile3IndustryPublicAdministration]=d.[Mile3IndustryPublicAdministration]
      ,[Mile1PctOfIncomeForMortgage]=d.[Mile1PctOfIncomeForMortgage]
      ,[Mile1GrowthRateOwnerOccHUs]=d.[Mile1GrowthRateOwnerOccHUs]
      ,[Mile1CivPop16PlusLaborForce]=d.[Mile1CivPop16PlusLaborForce]
      ,[Mile1EmployedCivilianPop16Plus]=d.[Mile1EmployedCivilianPop16Plus]
      ,[Mile1UnemployedPopulation16Plus]=d.[Mile1UnemployedPopulation16Plus]
      ,[Mile1EmployedCivilianPop16To24]=d.[Mile1EmployedCivilianPop16To24]
      ,[Mile1UnemployedPop16To24]=d.[Mile1UnemployedPop16To24]
      ,[Mile1UnempRatePop16To24]=d.[Mile1UnempRatePop16To24]
      ,[Mile1EmployedCivilianPop25To54]=d.[Mile1EmployedCivilianPop25To54]
      ,[Mile1UnemployedPop25To54]=d.[Mile1UnemployedPop25To54]
      ,[Mile1EmployedCivilianPop55To64]=d.[Mile1EmployedCivilianPop55To64]
      ,[Mile1UnemployedPop55To64]=d.[Mile1UnemployedPop55To64]
      ,[Mile1EmployedCivilianPop65Plus]=d.[Mile1EmployedCivilianPop65Plus]
      ,[Mile1UnemployedPop65Plus]=d.[Mile1UnemployedPop65Plus]
      ,[Mile1Emp16PlusByIndustryBase]=d.[Mile1Emp16PlusByIndustryBase]
      ,[Mile1IndustryAgriculture]=d.[Mile1IndustryAgriculture]
      ,[Mile1IndustryConstruction]=d.[Mile1IndustryConstruction]
      ,[Mile1IndustryManufacturing]=d.[Mile1IndustryManufacturing]
      ,[Mile1IndustryWholesaleTrade]=d.[Mile1IndustryWholesaleTrade]
      ,[Mile1IndustryRetailTrade]=d.[Mile1IndustryRetailTrade]
      ,[Mile1IndustryTransportation]=d.[Mile1IndustryTransportation]
      ,[Mile1IndustryUtilities]=d.[Mile1IndustryUtilities]
      ,[Mile1IndustryInformation]=d.[Mile1IndustryInformation]
      ,[Mile1IndustryFinanceAndInsurance]=d.[Mile1IndustryFinanceAndInsurance]
      ,[Mile1IndustryRealEstate]=d.[Mile1IndustryRealEstate]
      ,[Mile1IndustryProfessionalAndTechSvcs]=d.[Mile1IndustryProfessionalAndTechSvcs]
      ,[Mile1IndustryAdminAndWasteMgmt]=d.[Mile1IndustryAdminAndWasteMgmt]
      ,[Mile1IndustryHealthCare] = d.[Mile1IndustryHealthCare]
      ,[Mile1IndustryAccommodationAndFoodSvcs] =d.[Mile1IndustryAccommodationAndFoodSvcs]
      ,[Mile1IndustryOtherServices] = d.[Mile1IndustryOtherServices]
      ,[Mile1IndustryPublicAdministration] = d.[Mile1IndustryPublicAdministration]
--select *
From listing l 
join SellerBrokerage sb on l.SellerBrokerageId = sb.id
join Demographic d on l.ListingWebsiteId = d.BrokerWebsiteId
and d.BrokerWebsiteSource = sb.name
where d.Mile1IndustryPublicAdministration is not null
and l.Mile1IndustryPublicAdministration is null
END
GO
GRANT EXECUTE ON  [dbo].[DensifyDemographic] TO [JRW\etl_user]
GO
