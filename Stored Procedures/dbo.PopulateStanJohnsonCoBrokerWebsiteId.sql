SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Adam Eidson
-- Create date: 9/28/2020
-- Description:	Stored Procedure to populate BrokerWebsiteId for StanJohnsonCo, which does not supply a broker id from their website. 
-- This is used to keep track of what records have been inserted to SF, and which have not.
-- =============================================
CREATE PROCEDURE [dbo].[PopulateStanJohnsonCoBrokerWebsiteId]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

  --Populate Local Datastore Id where no website match exists
  Insert Into AcquisitionCandidateAggregator.dbo.StanJohnsonCo
  (
		[listing_name]
      ,[Price]
      ,[Cap_RateEquity]
      ,[details]
      ,[ViewDetails]
  )

  select stg.[listing_name]
      ,stg.[Price]
      ,stg.[Cap_RateEquity]
      ,stg.[details]
      ,stg.[ViewDetails]
  from AcquisitionCandidateAggregator.STAGE.StanJohnsonCo stg
  left join  AcquisitionCandidateAggregator.dbo.StanJohnsonCo loc on stg.[ViewDetails] = loc.[ViewDetails]
  where loc.listing_name is null

  --update stage table so other processes do not need to be updated to accommodate the local datastore. --AE
  update stg
  set  LocalListingId = loc.LocalListingId
  from AcquisitionCandidateAggregator.STAGE.StanJohnsonCo stg
  left join  AcquisitionCandidateAggregator.dbo.StanJohnsonCo loc on stg.[ViewDetails] = loc.[ViewDetails]
  where loc.LocalListingId is not null
  and stg.LocalListingId is null
END
GO
GRANT EXECUTE ON  [dbo].[PopulateStanJohnsonCoBrokerWebsiteId] TO [JRW\etl_user]
GO
