SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 12/17/2020
-- Description:	Stored Procedure to populate BrokerWebsiteId for LemRX, which does not supply a broker id from their website. 
-- This is used to keep track of what records have been inserted to SF, and which have not.
-- =============================================
--exec AcquisitionCandidateAggregator.[dbo].[PopulateLemRXBrokerWebsiteId]
CREATE PROCEDURE [dbo].[PopulateLemRXBrokerWebsiteId]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

  --Populate Local Datastore Id where no website match exists
  Insert Into AcquisitionCandidateAggregator.dbo.LemRX
  ([ListingName] ,
	[FullAddress] ,
	[Price] ,
	[CapRate] ,
	[ListingURL]
  )

  select stg.[ListingName] ,
	stg.[FullAddress] ,
	stg.[Price] ,
	stg.[CapRate] ,
	stg.[ListingURL]
  from AcquisitionCandidateAggregator.STAGE.LemRX stg
  left join  AcquisitionCandidateAggregator.dbo.LemRX loc on stg.[ListingName] = loc.[ListingName]
  and stg.[FullAddress] = loc.[FullAddress]
  and ISNULL(stg.Price,'') = ISNULL(loc.Price,'')
  where loc.ListingURL is null

  --update stage table so other processes do not need to be updated to accommodate the local datastore. --AE
  update stg
  set  LocalListingId = loc.LocalListingId
  from AcquisitionCandidateAggregator.STAGE.LemRX stg
  left join  AcquisitionCandidateAggregator.dbo.LemRX loc on stg.[ListingName] = loc.[ListingName]
  and stg.[FullAddress] = loc.[FullAddress]
  and ISNULL(stg.Price,'') = ISNULL(loc.Price,'')
  where loc.LocalListingId is not null
  and stg.LocalListingId is null
END

GO
GRANT EXECUTE ON  [dbo].[PopulateLemRXBrokerWebsiteId] TO [JRW\etl_user]
GO
