SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC AcquisitionCandidateaggregator.dbo.SFListingWithoutTenantAssociation
CREATE PROCEDURE [dbo].[SFListingWithoutTenantAssociation] 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @MemOptVarTab DistinctAliasMemOptTableVar ;
       INSERT @MemOptVarTab ([value], SF_Tenant_Id)
 --   SELECT
 --       p.[ProductID], p.[Name]
 --   FROM Production.Product AS p
 --      SELECT * FROM @MemOptVarTab
	--create table #temp_DistinctAlias
	--(value varchar(1000),
	--SF_Tenant_Id varchar(50)
	--)
	--insert 	into #temp_DistinctAlias
--select value
--	, Id as SF_Tenant_Id
-- from [SalesForce_Reporting].[dbo].[vTenant_ApprovedAggregator]
-- CROSS APPLY STRING_SPLIT(Aliases__c,';') 
--  where [Approved_Aggregator__c] = 1

---------------------------------------
-------------------------------------- 

--drop table #temp_vPropertyTenantAssociation
--drop table #temp_alias
select value
	, Id as SF_Tenant_Id
	--into #temp_alias
 from [SalesForce_Reporting].[dbo].[vTenant_ApprovedAggregator]
 CROSS APPLY STRING_SPLIT(Aliases__c,';') 
  where [Approved_Aggregator__c] = 1


  DECLARE @PropertyTenantMemOptVarTab SFPropertyTenantAssociationMemOptTableVar ;
       INSERT @PropertyTenantMemOptVarTab (Id, PropertyId, TenantId)
select Id, 
Property__c,
Tenant__c
--into #temp_vPropertyTenantAssociation
From Salesforce_Reporting.dbo.vPropertyTenantAssociation

  DECLARE @PropertyMemOptVarTab SFPropertyMemOptTableVar ;
       INSERT @PropertyMemOptVarTab (Id, Name, Listingwebsiteid, ListingURL)
select Id,
Name,
Listing_Website_Id__c,
Listing_URL__c
--into #temp_Property
from Salesforce_Reporting.dbo.vPropertyWithListingURL

 select distinct x.*--, v.SF_Tenant_Id
  from(
 select * from @MemOptVarTab
) as v (Aliases__c, SF_Tenant_Id)


CROSS APPLY 
( 
select distinct SF_Listing.id as Property__c,
v.SF_Tenant_Id as Tenant__c
from @PropertyMemOptVarTab SF_Listing
join AcquisitionCandidateAggregator.dbo.listing aca on SF_Listing.ListingWebsiteId = aca.ListingWebsiteId 
																			and SF_Listing.Name = SUBSTRING(aca.[Name],0,80)
																			--aca.ListingWebsiteId = SF_Listing.Listing_Website_Id__c
join AcquisitionCandidateAggregator.dbo.ListingTenant lt on lt.ListingId = aca.id
--join dbo.vTenant_approvedAggregator SF_Tenant on SF_Tenant.Aliases__c = lt.TenantName
left join @PropertyTenantMemOptVarTab vpta on vpta.PropertyId = SF_Listing.id
and vpta.TenantId = v.SF_Tenant_Id
where vpta.id is null
and (aca.name  like v.Aliases__c
   or lt.TenantName like v.Aliases__c)

   ) as x

END
GO
GRANT EXECUTE ON  [dbo].[SFListingWithoutTenantAssociation] TO [JRW\etl_user]
GO
