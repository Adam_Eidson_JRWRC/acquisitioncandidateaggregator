SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 11/13/2020
-- Description:	Stored Procedure to populate BrokerWebsiteId for Schuchert, which does not supply a broker id from their website. 
-- This is used to keep track of what records have been inserted to SF, and which have not.
-- =============================================
CREATE PROCEDURE [dbo].[PopulateSchuchertBrokerWebsiteId]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

  --Populate Local Datastore Id where no website match exists
  Insert Into AcquisitionCandidateAggregator.dbo.Schuchert
  (
		[Title]
      ,[Price]
      ,[ListingURL]
      ,[Street]
      ,[City]
      ,[State]
      ,[LeaseTerm]
      ,[CapRate]
      ,[YearBuilt]
      ,[LeaseExpiration]
      ,[ListingNOI]
  )

  select stg.[Title]
      ,stg.[Price]
      ,stg.[ListingURL]
      ,stg.[Street]
      ,stg.[City]
      ,stg.[State]
      ,stg.[LeaseTerm]
      ,stg.[CapRate]
      ,stg.[YearBuilt]
      ,stg.[LeaseExpiration]
      ,stg.[ListingNOI]
  from AcquisitionCandidateAggregator.STAGE.Schuchert stg
  left join  AcquisitionCandidateAggregator.dbo.Schuchert loc on stg.[Title] = loc.[Title]
   and isnull(stg.[Street],'') = isnull(loc.[Street],'')
  and isnull(stg.price,'')= isnull(loc.price,'')
  where loc.ListingURL is null

  --update stage table so other processes do not need to be updated to accommodate the local datastore. --AE
  update stg
  set  LocalListingId = loc.LocalListingId
  from AcquisitionCandidateAggregator.STAGE.Schuchert stg
  left join  AcquisitionCandidateAggregator.dbo.Schuchert loc on stg.[Title] = loc.[Title]
  and isnull(stg.[Street],'') = isnull(loc.[Street],'')
  and isnull(stg.price,'')= isnull(loc.price,'')
  where loc.LocalListingId is not null
  and stg.LocalListingId is null
END
GO
GRANT EXECUTE ON  [dbo].[PopulateSchuchertBrokerWebsiteId] TO [JRW\etl_user]
GO
