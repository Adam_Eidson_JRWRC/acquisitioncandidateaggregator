SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 11/13/2020
-- Description:	Stored Procedure to populate BrokerWebsiteId for NetLeaseAdvisoryGroup, which does not supply a broker id from their website. 
-- This is used to keep track of what records have been inserted to SF, and which have not.
-- =============================================
CREATE PROCEDURE [dbo].[PopulateNetLeaseAdvisoryGroupBrokerWebsiteId]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

  --Populate Local Datastore Id where no website match exists
  Insert Into AcquisitionCandidateAggregator.dbo.NetLeaseAdvisoryGroup
  (
		[ListingName] ,
	[Price],
	[CapRate],
	[NOI],
	[Latitude],
	[Longitude] ,
	[ListingURL]
  )

  select stg.[ListingName]
      ,stg.[Price]
      ,stg.[CapRate]
      ,stg.[NOI]
      ,stg.[Latitude]
      ,stg.[Longitude]
	  ,stg.[ListingURL]
  from AcquisitionCandidateAggregator.STAGE.NetLeaseAdvisoryGroup stg
  left join  AcquisitionCandidateAggregator.dbo.NetLeaseAdvisoryGroup loc on stg.[ListingName] = loc.[ListingName]
  and stg.[Latitude] = loc.[Latitude]
  and stg.[Longitude] = loc.[Longitude]
  and isnull(stg.Price,0) = isnull(loc.Price,0)
  where loc.ListingURL is null

  --update stage table so other processes do not need to be updated to accommodate the local datastore. --AE
  update stg
  set  LocalListingId = loc.LocalListingId
  from AcquisitionCandidateAggregator.STAGE.NetLeaseAdvisoryGroup stg
  left join  AcquisitionCandidateAggregator.dbo.NetLeaseAdvisoryGroup loc on stg.[ListingName] = loc.[ListingName]
  and stg.[Latitude] = loc.[Latitude]
  and stg.[Longitude] = loc.[Longitude]
  and isnull( stg.Price ,0)= isnull(loc.Price,0)
  where loc.LocalListingId is not null
  and stg.LocalListingId is null
END




GO
GRANT EXECUTE ON  [dbo].[PopulateNetLeaseAdvisoryGroupBrokerWebsiteId] TO [JRW\etl_user]
GO
