SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 10/12/2020
-- Description:	Stored Procedure meant to be called by Python to run SSIS Package to move data from STAGE table to DBO.
-- =============================================
--exec AcquisitionCandidateAggregator.dbo.ExecuteSSISOrderOfOperations
CREATE PROCEDURE [dbo].[ExecuteSSISOrderOfOperations]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Declare @execution_id bigint
EXEC [SSISDB].[catalog].[create_execution] @package_name=N'Order Of Operations.dtsx', @execution_id=@execution_id OUTPUT, @folder_name=N'AcquisitionCandidateAggregator', @project_name=N'Acquisition Candidate Aggregator Data Prep', @use32bitruntime=False, @reference_id=Null, @runinscaleout=False
Select @execution_id
DECLARE @var0 smallint = 1
EXEC [SSISDB].[catalog].[set_execution_parameter_value] @execution_id,  @object_type=50, @parameter_name=N'LOGGING_LEVEL', @parameter_value=@var0
EXEC [SSISDB].[catalog].[start_execution] @execution_id
END
GO
GRANT EXECUTE ON  [dbo].[ExecuteSSISOrderOfOperations] TO [JRW\etl_user]
GO
