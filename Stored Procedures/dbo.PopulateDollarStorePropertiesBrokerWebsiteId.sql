SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 4/23/2020
-- Description:	Stored Procedure to populate BrokerWebsiteId for Dollar Store Properties, which does not supply a broker id from their website. 
-- This is used to keep track of what records have been inserted to SF, and which have not.
-- =============================================
--exec [PopulateDollarStorePropertiesBrokerWebsiteId]
CREATE PROCEDURE [dbo].[PopulateDollarStorePropertiesBrokerWebsiteId]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

  --Populate Local Datastore Id where no website match exists
  Insert Into AcquisitionCandidateAggregator.dbo.DollarStoreProperties
  (
		[ListingName] ,
		[Address],
	[Price],
	[CapRate],
	[ListingURL]
  )

  select stg.[ListingName]
  ,stg.[Address]
      ,stg.[Price]
      ,stg.[CapRate]
	  ,stg.[ListingURL]
  from AcquisitionCandidateAggregator.STAGE.DollarStoreProperties stg
  left join  AcquisitionCandidateAggregator.dbo.DollarStoreProperties loc on stg.[ListingName] = loc.[ListingName]
  and stg.Price = loc.Price
  and stg.[Address] = loc.address
  where loc.ListingURL is null

  --update stage table so other processes do not need to be updated to accommodate the local datastore. --AE
  update stg
  set  LocalListingId = loc.LocalListingId
  from AcquisitionCandidateAggregator.STAGE.DollarStoreProperties stg
  left join  AcquisitionCandidateAggregator.dbo.DollarStoreProperties loc on stg.[ListingName] = loc.[ListingName]
  and stg.[Address] = loc.address
  and isnull( stg.Price ,0)= isnull(loc.Price,0)
  where loc.LocalListingId is not null
  and stg.LocalListingId is null
END




GO
GRANT EXECUTE ON  [dbo].[PopulateDollarStorePropertiesBrokerWebsiteId] TO [JRW\etl_user]
GO
