SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 11/11/2020
-- Description:	Stored Procedure to populate BrokerWebsiteId for SullivanWickley, which does not supply a broker id from their website. 
-- This is used to keep track of what records have been inserted to SF, and which have not.
-- =============================================
CREATE PROCEDURE [dbo].[PopulateSullivanWickleyBrokerWebsiteId]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

  --Populate Local Datastore Id where no website match exists
  Insert Into AcquisitionCandidateAggregator.dbo.SullivanWickleyList
  (
		[Title] ,
	[State] ,
	[City] ,
	[Price] ,
	[CapRate] ,
	[LeaseType]
  )

  select stg.[Title]
      , stg.[State]
      , stg.[City]
      , stg.[Price]
      , stg.[CapRate]
      , stg.[LeaseType]
  from AcquisitionCandidateAggregator.STAGE.SullivanWickleyList stg
  left join  AcquisitionCandidateAggregator.dbo.SullivanWickleyList loc on stg.Title = loc.Title
  where loc.Title is null

  --update stage table so other processes do not need to be updated to accommodate the local datastore. --AE
  update stg
  set  LocalListingId = loc.LocalListingId
  from AcquisitionCandidateAggregator.STAGE.SullivanWickleyList stg
  left join  AcquisitionCandidateAggregator.dbo.SullivanWickleyList loc on stg.Title = loc.Title
  where loc.LocalListingId is not null
  and stg.LocalListingId is null
END

GO
GRANT EXECUTE ON  [dbo].[PopulateSullivanWickleyBrokerWebsiteId] TO [JRW\etl_user]
GO
