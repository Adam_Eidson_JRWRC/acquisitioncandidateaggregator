SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 6/1/2021
-- Description:	Replacement of view vBrokerWEbsiteDataApprovedTenant intended to take advantage of temp tables and indexes (as needed).
-- =============================================
--EXEC AcquisitionCandidateAggregator.dbo.BrokerWebsiteDataApprovedTenant 
--Drop table #temp_DistinctAlias
CREATE PROCEDURE [dbo].[BrokerWebsiteDataApprovedTenant] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;
	
	DECLARE @MemOptVarTab DistinctAliasMemOptTableVar ;
       INSERT @MemOptVarTab ([value], SF_Tenant_Id)
 --   SELECT
 --       p.[ProductID], p.[Name]
 --   FROM Production.Product AS p
 --      SELECT * FROM @MemOptVarTab
	--create table #temp_DistinctAlias
	--(value varchar(1000),
	--SF_Tenant_Id varchar(50)
	--)
	--insert 	into #temp_DistinctAlias
select value
	, Id as SF_Tenant_Id
 from [SalesForce_Reporting].[dbo].[vTenant_ApprovedAggregator]
 CROSS APPLY STRING_SPLIT(Aliases__c,';') 
  where [Approved_Aggregator__c] = 1

  
 select distinct x.*, v.SF_Tenant_Id
  from(
 select * from @MemOptVarTab
) as v (Aliases__c, SF_Tenant_Id)

CROSS APPLY 
( SELECT distinct l.*,
sb.name as sellerBrokerageName,
lbor.Name as [BrokerOfRecordName],
lbor.email as BrokerOfRecordEmail,
lbor.License as BrokerOfRecordLicense,
lbor.phone as BrokerOfRecordPhone,
lt.ImprovedTenantCapture
  FROM [AcquisitionCandidateAggregator].[dbo].[Listing] l
  left join [AcquisitionCandidateAggregator].[dbo].SellerBrokerage sb on sb.id = l.sellerbrokerageid
  left join [AcquisitionCandidateAggregator].[dbo].ListingBrokerOfRecord lbor on lbor.ListingId = l.id
   left join [AcquisitionCandidateAggregator].[dbo].ListingTenant lt on lt.ListingId = l.id
   where (l.name  like v.Aliases__c
   or lt.TenantName like v.Aliases__c)

   ) as x
END
GO
GRANT EXECUTE ON  [dbo].[BrokerWebsiteDataApprovedTenant] TO [JRW\etl_user]
GO
