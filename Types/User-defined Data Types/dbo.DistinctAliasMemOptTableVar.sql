CREATE TYPE [dbo].[DistinctAliasMemOptTableVar] AS TABLE
(
[value] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SF_Tenant_Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
INDEX [IX_DistinctAliasMemOptTableVar_ID] NONCLUSTERED ([value])
)
WITH
(
MEMORY_OPTIMIZED = ON
)
GO
GRANT CONTROL ON TYPE:: [dbo].[DistinctAliasMemOptTableVar] TO [JRW\etl_user]
GO
GRANT REFERENCES ON TYPE:: [dbo].[DistinctAliasMemOptTableVar] TO [JRW\etl_user]
GO
