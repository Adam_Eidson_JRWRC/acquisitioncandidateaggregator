CREATE TYPE [dbo].[SFPropertyMemOptTableVar] AS TABLE
(
[Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingWebsiteId] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingURL] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
INDEX [IX_DistinctAliasMemOptTableVar_ID] NONCLUSTERED ([Id], [ListingWebsiteId])
)
WITH
(
MEMORY_OPTIMIZED = ON
)
GO
