CREATE TYPE [dbo].[SFPropertyTenantAssociationMemOptTableVar] AS TABLE
(
[Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PropertyId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenantId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
INDEX [IX_DistinctAliasMemOptTableVar_ID] NONCLUSTERED ([PropertyId])
)
WITH
(
MEMORY_OPTIMIZED = ON
)
GO
