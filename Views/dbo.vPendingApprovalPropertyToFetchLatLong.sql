SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO













/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[vPendingApprovalPropertyToFetchLatLong] as 
SELECT distinct  b.id as ListingId
      
      ,[Street]+' '+isnull([city],'')+' '+isnull([state],'')+' '+isnull([Zip],'') as Address
	  ,[Street]
      ,[City]
      ,[State]
      ,[Zip] as PostalCode
  FROM [AcquisitionCandidateAggregator].[dbo].[vBrokerWebsiteDataPendingApprovalTenant] b
  
  
  where 
  isnull(street,'') != ''
  --and City is not null
  and Latitude is null
  and active =  1
GO
