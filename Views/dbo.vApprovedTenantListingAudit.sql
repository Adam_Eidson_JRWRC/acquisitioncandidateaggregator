SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vApprovedTenantListingAudit] as 
with cte_Listing as (

 select distinct x.*
  from(
 select Name , NormalizedName
 from ApprovedTenant
 --where active = 1
) as v (Name, NormalizedName)

CROSS APPLY 
( SELECT distinct l.*,
sb.name as sellerBrokerageName,
lbor.Name as [BrokerOfRecordName],
lbor.email as BrokerOfRecordEmail,
lbor.License as BrokerOfRecordLicense,
lbor.phone as BrokerOfRecordPhone,
v.normalizedName as Tenant
  FROM [AcquisitionCandidateAggregator].[dbo].[Listing] l
  left join [AcquisitionCandidateAggregator].[dbo].SellerBrokerage sb on sb.id = l.sellerbrokerageid
  left join [AcquisitionCandidateAggregator].[dbo].ListingBrokerOfRecord lbor on lbor.ListingId = l.id
   where l.name  like v.Name) as x
), cte_nonTSCOListing as (
SELECT distinct SUBSTRING(Listing.[Name],0,80) as Name
      ,convert(varchar(15),Listing.ContractPrice) as Contract_Price__c
	  ,case when ContractPrice>0
	  then NOI/ContractPrice
	  end as CapRate
      --,[CapRate] as Current_Cap_Rate__c
      ,convert(varchar(15),Listing.noi) as tCurrent_NOI__c
	  ,convert(varchar(15),Listing.noi) as Purchase_NOI__c
      ,convert(varchar(15),Listing.SqFt) as Sq_Ft__c
      --,cast([Years Remaining On Lease] as varchar(15)) as Current_Term_Remaining_years__c --formula field
     -- ,[Lot Size] as Lot_Size_Sq_Ft__c --formula field
      ,SUBSTRING(TRIM(Listing.[YearBuilt]),0,5) as Year_Built__c
      ,Listing.street as Street__c
      ,Listing.[city] as City__c
      ,case when len(Listing.[state]) > 2
	  then null
	  else Listing.[state] 
	  end as State__c
      ,cast(Listing.zip as varchar(50)) as Zip__c
      ,Listing.ListingURL as Listing_URL__c
     -- ,[BrokerWebsiteSource] as Seller_Brokerage__c
      --,[PropertyType] Field Needs to be created
      --,[PropertySubType] Field needs to be created
      ,Listing.TypeOfOwnership as Type_of_Ownership__c
      ,cast(Listing.[YearRenovated] as varchar(15)) as Year_Renovated__c
      --,[LeaseOptions]
      --,[TenantCredit]
      ,Listing.CurrentTermExpirationDate as Current_Term_Expiration_Date__c
	  ,Listing.NetLeaseType as NN_or_NNN__c
	  ,'Listing-Active' as Stage__c
	  ,'a1W0b000007SYyM' as Portfolio__c,
	  vba.id as Seller_Agent__c,
	  vps.id  as Seller_Brokerage__c,
	  case when Listing.ListingCreatedDate = ''
	  then null
	  else convert(varchar(20),CAST(Listing.ListingCreatedDate as smalldatetime),126) 
	   end as Listing_Created_Date__c,
	   Listing.ListingWebsiteId as Listing_Website_Id__c,
	   convert(varchar(50),Listing.CrimeRateIndex) as Crime_Rate_Index__c,
	   listing.tenant
  FROM cte_Listing Listing
  left join SalesForce_Reporting.dbo.vBrokerAgent vba on cast(isnull(vba.firstName, '') as varchar(50))+' '+cast(isnull(vba.lastName, '') as varchar(50)) = cast(isnull(Listing.[BrokerOfRecordName], '') as varchar(50))
  --left join SalesForce_Reporting.dbo.vTenant SF_tenant on SF_tenant.Name = Listing.Tenant
  left join SalesForce_Reporting.dbo.vPropertyWithListingURL SF_property on SF_Property.Listing_Website_Id__c = Listing.ListingWebsiteId 
																			--and SF_Property.Name = SUBSTRING(Listing.[Name],0,80)--and SF_property.Listing_URL__c = Listing.ListingURL
  --and isNull(convert(varchar(15),Listing.ContractPrice),0) = SF_Property.Contract_Price__c
  left join SalesForce_Reporting.dbo.vPropertySource vps on cast(isnull(vps.Name ,'')as varchar(50)) = cast(isnull(Listing.sellerBrokerageName , '')as varchar(50))
  --and SF_property.Listing_Website_Id__c = vps.id
  where SF_Property.id is null
  and (ISNULL([5MileRadiusPopulation],50000) >= 35000)
  --and vba.id is not null
  and listing.Name not like '%Tractor%'
  and (ISNULL(CrimeRateIndex,0)<=300)
  and listing.Name not like '%dark%'
  and listing.Name not like '%zero cash flow%'
  and listing.Name not like '%Outlot%'
  and listing.Name not like '%Outparcel%'
  and listing.Name not like '%Out parcel%'
  and listing.Name not like '%Carveout%'
  and (DATEDIFF(year, GETDATE(),listing.CurrentTermExpirationDate) >= 6
  or listing.CurrentTermExpirationDate is null)
  and (
  isnull(listing.[Mile1VacantHousingPercent],0) <= 15
  and isnull(listing.[Mile3VacantHousingPercent],0) <= 15
  and isnull(listing.[Mile5VacantHousingPercent],0) <= 15
  )
  
), cte_TSCOListing as (
SELECT distinct SUBSTRING(Listing.[Name],0,80) as Name
       ,convert(varchar(15),Listing.ContractPrice) as Contract_Price__c
	   ,case when ContractPrice>0
	  then NOI/ContractPrice
	  end as CapRate
      --,[CapRate] as Current_Cap_Rate__c
      ,convert(varchar(15),Listing.noi) as tCurrent_NOI__c
	  ,convert(varchar(15),Listing.noi) as Purchase_NOI__c
      ,convert(varchar(15),Listing.SqFt) as Sq_Ft__c
      --,cast([Years Remaining On Lease] as varchar(15)) as Current_Term_Remaining_years__c --formula field
     -- ,[Lot Size] as Lot_Size_Sq_Ft__c --formula field
      ,SUBSTRING(TRIM(Listing.[YearBuilt]),0,5) as Year_Built__c
      ,Listing.street as Street__c
      ,Listing.[city] as City__c
      ,case when len(Listing.[state]) > 2
	  then null
	  else Listing.[state] 
	  end as State__c
      ,cast(Listing.zip as varchar(50)) as Zip__c
      ,Listing.ListingURL as Listing_URL__c
     -- ,[BrokerWebsiteSource] as Seller_Brokerage__c
      --,[PropertyType] Field Needs to be created
      --,[PropertySubType] Field needs to be created
      ,Listing.TypeOfOwnership as Type_of_Ownership__c
      ,cast(Listing.[YearRenovated] as varchar(15)) as Year_Renovated__c
      --,[LeaseOptions]
      --,[TenantCredit]
      ,Listing.CurrentTermExpirationDate as Current_Term_Expiration_Date__c
	  ,Listing.NetLeaseType as NN_or_NNN__c
	  ,'Listing-Active' as Stage__c
	  ,'a1W0b000007SYyM' as Portfolio__c,
	  vba.id as Seller_Agent__c,
	  vps.id  as Seller_Brokerage__c,
	  case when Listing.ListingCreatedDate = ''
	  then null
	  else convert(varchar(20),CAST(Listing.ListingCreatedDate as smalldatetime),126) 
	   end as Listing_Created_Date__c,
	   Listing.ListingWebsiteId as Listing_Website_Id__c,
	   convert(varchar(50),Listing.CrimeRateIndex) as Crime_Rate_Index__c,
	   listing.tenant
  FROM cte_Listing Listing
  left join SalesForce_Reporting.dbo.vBrokerAgent vba on cast(isnull(vba.firstName, '') as varchar(50))+' '+cast(isnull(vba.lastName, '') as varchar(50)) = cast(isnull(Listing.[BrokerOfRecordName], '') as varchar(50))
  --left join SalesForce_Reporting.dbo.vTenant SF_tenant on SF_tenant.Name = Listing.Tenant
  left join SalesForce_Reporting.dbo.vPropertyWithListingURL SF_property on SF_Property.Listing_Website_Id__c = Listing.ListingWebsiteId
																			--and SF_Property.Name = SUBSTRING(Listing.[Name],0,80)
  left join SalesForce_Reporting.dbo.vPropertySource vps on cast(isnull(vps.Name ,'')as varchar(50)) = cast(isnull(Listing.sellerBrokerageName , '')as varchar(50))
  where SF_Property.id is null
  and (ISNULL([5MileRadiusPopulation],50000) >= 30000)
  --and vba.id is not null
  and Listing.Name like '%Tractor%'
  and (ISNULL(CrimeRateIndex,0)<=300)
  and listing.Name not like '%dark%'
  and listing.Name not like '%zero cash flow%'
  and listing.Name not like '%Outlot%'
  and listing.Name not like '%Outparcel%'
  and listing.Name not like '%Out parcel%'
  and listing.Name not like '%Carveout%'
  and (DATEDIFF(year, GETDATE(),listing.CurrentTermExpirationDate) >= 6
  or listing.CurrentTermExpirationDate is null)
  and (
  isnull(listing.[Mile1VacantHousingPercent],0) <= 15
  and isnull(listing.[Mile3VacantHousingPercent],0) <= 15
  and isnull(listing.[Mile5VacantHousingPercent],0) <= 15
  )
), cte_finalPrepped as (


select distinct *
from cte_nonTSCOListing
where ISNULL(CapRate,6)>=0.055
union
select distinct *
from cte_TSCOListing
where ISNULL(CapRate,6)>=0.055
)

select distinct tenant
from cte_finalPrepped
GO
