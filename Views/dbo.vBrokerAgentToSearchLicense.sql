SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vBrokerAgentToSearchLicense] as 
select distinct ba.Id, 
ba.FirstName,
ba.LastName
From AcquisitionCandidateAggregator.dbo.BrokerAgent ba
left join AcquisitionCandidateAggregator.dbo.BrokerAgentLicense bal on bal.BrokerAgentId = ba.id
where ba.CREXiSearched = 0
and bal.id is null
GO
