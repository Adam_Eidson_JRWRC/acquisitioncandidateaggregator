SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE View [dbo].[vListingWithoutTenantAssociation] as
Select  distinct l.id as ListingId,
l.Latitude,
l.Longitude
from dbo.listing L
left join dbo.ListingTenant lt on lt.ListingId = l.id
left join dbo.vBrokerWebsiteDataApprovedTenant bwda with (NOLOCK) on bwda.ListingWebsiteId = l.ListingWebsiteId
where lt.id is null
and bwda.ListingWebsiteId is null
and l.active = 1
and l.Latitude is not null
GO
