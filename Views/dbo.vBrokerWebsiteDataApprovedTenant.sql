SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

































CREATE View [dbo].[vBrokerWebsiteDataApprovedTenant] as 

Select * 
From OPENQUERY([JRW-DS01],'EXECUTE AcquisitionCandidateAggregator.dbo.BrokerWebsiteDataApprovedTenant WITH RESULT SETS (([id] [int] ,
	[Name] [varchar](80) ,
	[ListingURL] [varchar](255) ,
	[ContractPrice] [decimal](18, 2) ,
	[SellerBrokerageId] [int]  ,
	[NOI] [decimal](18, 2) ,
	[SqFt] [numeric](18, 0) ,
	[YearBuilt] [varchar](4) ,
	[Street] [varchar](255) ,
	[City] [varchar](255) ,
	[State] [varchar](2) ,
	[Zip] [varchar](11) ,
	[TypeOfOwnership] [varchar](45) ,
	[yearRenovated] [varchar](4) ,
	[CurrentTermExpirationDate] [date] ,
	[NetLeaseType] [varchar](15) ,
	[1MileRadiusPopulation] [numeric](18, 0) ,
	[MedianHouseholdIncome] [decimal](18, 2) ,
	[1MileRadiusVacantHousing] [numeric](18, 0) ,
	[1MileRadiusOwnerOccupiedHousing] [numeric](18, 0) ,
	[1MileRadiusRenterOccupiedHousing] [numeric](18, 0) ,
	[1MileRadiusAvgHHIncome] [numeric](18, 0) ,
	[3MileRadiusPopulation] [numeric](18, 0) ,
	[3MileRadiusVacantHousing] [numeric](18, 0) ,
	[3MileRadiusOwnerOccupiedHousing] [numeric](18, 0) ,
	[3MileRadiusRenterOccupiedHousing] [numeric](18, 0) ,
	[3MileRadiusAvgHHIncome] [numeric](18, 0) ,
	[5MileRadiusPopulation] [numeric](18, 0) ,
	[5MileRadiusVacantHousing] [numeric](18, 0) ,
	[5MileRadiusOwnerOccupiedHousing] [numeric](18, 0) ,
	[5MileRadiusRenterOccupiedHousing] [numeric](18, 0) ,
	[CrimeRateIndex] [numeric](6, 2) ,
	[ListingCreatedDate] [datetime] ,
	[ListingWebsiteId] [varchar](100) ,
	[Active] [int] ,
	[Latitude] [decimal](12, 9) ,
	[Longitude] [decimal](12, 9) ,
	[Mile5PctOfIncomeForMortgage] [decimal](5, 2) ,
	[Mile5GrowthRateOwnerOccHUs] [decimal](5, 2) ,
	[Mile5CivPop16PlusLaborForce] [int] ,
	[Mile5EmployedCivilianPop16Plus] [int] ,
	[Mile5UnemployedPopulation16Plus] [int] ,
	[Mile5EmployedCivilianPop16To24] [int] ,
	[Mile5UnemployedPop16To24] [int] ,
	[Mile5UnempRatePop16To24] [decimal](5, 2) ,
	[Mile5EmployedCivilianPop25To54] [int] ,
	[Mile5UnemployedPop25To54] [int] ,
	[Mile5EmployedCivilianPop55To64] [int] ,
	[Mile5UnemployedPop55To64] [int] ,
	[Mile5EmployedCivilianPop65Plus] [int] ,
	[Mile5UnemployedPop65Plus] [int] ,
	[Mile5Emp16PlusByIndustryBase] [int] ,
	[Mile5IndustryAgriculture] [int] ,
	[Mile5IndustryConstruction] [int] ,
	[Mile5IndustryManufacturing] [int] ,
	[Mile5IndustryWholesaleTrade] [int] ,
	[Mile5IndustryRetailTrade] [int] ,
	[Mile5IndustryTransportation] [int] ,
	[Mile5IndustryUtilities] [int] ,
	[Mile5IndustryInformation] [int] ,
	[Mile5IndustryFinanceAndInsurance] [int] ,
	[Mile5IndustryRealEstate] [int] ,
	[Mile5IndustryProfessionalAndTechSvcs] [int] ,
	[Mile5IndustryAdminAndWasteMgmt] [int] ,
	[Mile5IndustryHealthCare] [int] ,
	[Mile5IndustryAccommodationAndFoodSvcs] [int] ,
	[Mile5IndustryOtherServices] [int] ,
	[Mile5IndustryPublicAdministration] [int] ,
	[Mile3PctOfIncomeForMortgage] [decimal](5, 2) ,
	[Mile3GrowthRateOwnerOccHUs] [decimal](5, 2) ,
	[Mile3CivPop16PlusLaborForce] [int] ,
	[Mile3EmployedCivilianPop16Plus] [int] ,
	[Mile3UnemployedPopulation16Plus] [int] ,
	[Mile3EmployedCivilianPop16To24] [int] ,
	[Mile3UnemployedPop16To24] [int] ,
	[Mile3UnempRatePop16To24] [decimal](5, 2) ,
	[Mile3EmployedCivilianPop25To54] [int] ,
	[Mile3UnemployedPop25To54] [int] ,
	[Mile3EmployedCivilianPop55To64] [int] ,
	[Mile3UnemployedPop55To64] [int] ,
	[Mile3EmployedCivilianPop65Plus] [int] ,
	[Mile3UnemployedPop65Plus] [int] ,
	[Mile3Emp16PlusByIndustryBase] [int] ,
	[Mile3IndustryAgriculture] [int] ,
	[Mile3IndustryConstruction] [int] ,
	[Mile3IndustryManufacturing] [int] ,
	[Mile3IndustryWholesaleTrade] [int] ,
	[Mile3IndustryRetailTrade] [int] ,
	[Mile3IndustryTransportation] [int] ,
	[Mile3IndustryUtilities] [int] ,
	[Mile3IndustryInformation] [int] ,
	[Mile3IndustryFinanceAndInsurance] [int] ,
	[Mile3IndustryRealEstate] [int] ,
	[Mile3IndustryProfessionalAndTechSvcs] [int] ,
	[Mile3IndustryAdminAndWasteMgmt] [int] ,
	[Mile3IndustryHealthCare] [int] ,
	[Mile3IndustryAccommodationAndFoodSvcs] [int] ,
	[Mile3IndustryOtherServices] [int] ,
	[Mile3IndustryPublicAdministration] [int] ,
	[Mile1PctOfIncomeForMortgage] [decimal](5, 2) ,
	[Mile1GrowthRateOwnerOccHUs] [decimal](5, 2) ,
	[Mile1CivPop16PlusLaborForce] [int] ,
	[Mile1EmployedCivilianPop16Plus] [int] ,
	[Mile1UnemployedPopulation16Plus] [int] ,
	[Mile1EmployedCivilianPop16To24] [int] ,
	[Mile1UnemployedPop16To24] [int] ,
	[Mile1UnempRatePop16To24] [decimal](5, 2) ,
	[Mile1EmployedCivilianPop25To54] [int] ,
	[Mile1UnemployedPop25To54] [int] ,
	[Mile1EmployedCivilianPop55To64] [int] ,
	[Mile1UnemployedPop55To64] [int] ,
	[Mile1EmployedCivilianPop65Plus] [int] ,
	[Mile1UnemployedPop65Plus] [int] ,
	[Mile1Emp16PlusByIndustryBase] [int] ,
	[Mile1IndustryAgriculture] [int] ,
	[Mile1IndustryConstruction] [int] ,
	[Mile1IndustryManufacturing] [int] ,
	[Mile1IndustryWholesaleTrade] [int] ,
	[Mile1IndustryRetailTrade] [int] ,
	[Mile1IndustryTransportation] [int] ,
	[Mile1IndustryUtilities] [int] ,
	[Mile1IndustryInformation] [int] ,
	[Mile1IndustryFinanceAndInsurance] [int] ,
	[Mile1IndustryRealEstate] [int] ,
	[Mile1IndustryProfessionalAndTechSvcs] [int] ,
	[Mile1IndustryAdminAndWasteMgmt] [int] ,
	[Mile1IndustryHealthCare] [int] ,
	[Mile1IndustryAccommodationAndFoodSvcs] [int] ,
	[Mile1IndustryOtherServices] [int] ,
	[Mile1IndustryPublicAdministration] [int] ,
	[Mile1TotalHousingUnit] [int] ,
	[Mile3TotalHousingUnit] [int] ,
	[Mile5TotalHousingUnit] [int] ,
	[Mile3MedianHHIncome] [numeric](18, 0) ,
	[Mile5MedianHHIncome] [numeric](18, 0) ,
	[Mile5AverageHHIncome] [numeric](18, 0) ,
	[Mile1VacantHousingPercent] [numeric](5, 2) ,
	[Mile3VacantHousingPercent] [numeric](5, 2) ,
	[Mile5VacantHousingPercent] [numeric](5, 2) ,
	[Mile5AnnualPopGrowthPercentTrailing10] [numeric](5, 2) ,
	[Mile3AnnualPopGrowthPercentTrailing10] [numeric](5, 2) ,
	[Mile1AnnualPopGrowthPercentTrailing10] [numeric](5, 2) ,
	[Mile1AnnualPopGrowthPercentProjected5] [numeric](5, 2) ,
	[Mile3AnnualPopGrowthPercentProjected5] [numeric](5, 2) ,
	[Mile5AnnualPopGrowthPercentProjected5] [numeric](5, 2) ,
	[VehicleCount] [int],
	sellerBrokerageName varchar(255),
	BrokerOfRecordName varchar(250),
	BrokerOfRecordEmail varchar(50),
	BrokerOfRecordLicense varchar(120),
	BrokerOfRecordPhone varchar(35),
	ImprovedTenantCapture varchar(5),
	SF_Tenant_Id varchar(20)
	))')


--with cte_distinctAlias as (
--select value
--	, Id as SF_Tenant_Id
-- from [SalesForce_Reporting].[dbo].[vTenant_ApprovedAggregator]


-- CROSS APPLY STRING_SPLIT(Aliases__c,';') 
--  where [Approved_Aggregator__c] = 1
--)
  
-- select distinct x.*, v.SF_Tenant_Id
--  from(
-- select * from cte_distinctAlias
--) as v (Aliases__c, SF_Tenant_Id)

--CROSS APPLY 
--( SELECT distinct l.*,
--sb.name as sellerBrokerageName,
--lbor.Name as [BrokerOfRecordName],
--lbor.email as BrokerOfRecordEmail,
--lbor.License as BrokerOfRecordLicense,
--lbor.phone as BrokerOfRecordPhone,
--lt.ImprovedTenantCapture
--  FROM [AcquisitionCandidateAggregator].[dbo].[Listing] l
--  left join [AcquisitionCandidateAggregator].[dbo].SellerBrokerage sb on sb.id = l.sellerbrokerageid
--  left join [AcquisitionCandidateAggregator].[dbo].ListingBrokerOfRecord lbor on lbor.ListingId = l.id
--   left join [AcquisitionCandidateAggregator].[dbo].ListingTenant lt on lt.ListingId = l.id
--   where (l.name  like v.Aliases__c
--   or lt.TenantName like v.Aliases__c)

--   ) as x
--Original query. Updated 5/25/2021 AE
-- select distinct x.*
--  from(
-- select Name 
-- from ApprovedTenant
-- where active = 1
--) as v (Name)

--CROSS APPLY 
--( SELECT distinct l.*,
--sb.name as sellerBrokerageName,
--lbor.Name as [BrokerOfRecordName],
--lbor.email as BrokerOfRecordEmail,
--lbor.License as BrokerOfRecordLicense,
--lbor.phone as BrokerOfRecordPhone
--  FROM [AcquisitionCandidateAggregator].[dbo].[Listing] l
--  left join [AcquisitionCandidateAggregator].[dbo].SellerBrokerage sb on sb.id = l.sellerbrokerageid
--  left join [AcquisitionCandidateAggregator].[dbo].ListingBrokerOfRecord lbor on lbor.ListingId = l.id
--   left join [AcquisitionCandidateAggregator].[dbo].ListingTenant lt on lt.ListingId = l.id
--   where (l.name  like v.Name
--   or lt.TenantName like v.Name)

--   ) as x

GO
