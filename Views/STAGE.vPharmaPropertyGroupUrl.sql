SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [STAGE].[vPharmaPropertyGroupUrl] as
select distinct ListingId,
ListingId+'-'+ListingName+'.pdf' as pdf,
ListingURL as PDF_Url
From AcquisitionCandidateAggregator.STAGE.PharmaPropertyGroup
GO
