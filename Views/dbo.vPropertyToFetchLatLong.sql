SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [dbo].[vPropertyToFetchLatLong] as 
SELECT distinct  b.id as ListingId
      
      ,[Street]+' '+isnull([city],'')+' '+isnull([state],'')+' '+isnull([Zip],'') as Address
	  ,[Street]
      ,[City]
      ,[State]
      ,[Zip] as PostalCode
  FROM [AcquisitionCandidateAggregator].[dbo].Listing b
  
  
  where 
  isnull(street,'') != ''
  --and City is not null
  and active = 1
  and Latitude is null
GO
