SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE View [dbo].[vPropertyToReverseGeoLookup] as 
select id,
Latitude,
Longitude
From AcquisitionCandidateAggregator.dbo.listing l
where 
(Street is null 
or city is null)
and latitude is not null
and active = 1

GO
