SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO













/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[vPropertyToFetchDemographics] as 
SELECT distinct  b.[ListingWebsiteId] as BrokerWebsiteId,
b.sellerBrokerageName as BrokerWebsiteSource
	  ,Latitude
	  ,Longitude
  FROM [AcquisitionCandidateAggregator].[dbo].[vBrokerWebsiteDataApprovedTenant] b
--  left join [AcquisitionCandidateAggregator].[dbo].[Demographic] d on d.BrokerWebsiteId= b.[ListingWebsiteId] 
																	--and cast(isnull(d.brokerWebsiteSource,'') as varchar(150)) = cast(isnull(b.sellerBrokerageName,'') as varchar(150))
  --left join SalesForce_Reporting.dbo.vPropertyWithListingURL SF_prop on SF_prop.Listing_URL__c = b.ListingURL
  where latitude is not null
  and b.[1MileRadiusPopulation] is null
 -- and d.brokerWebsiteId is null
 -- and SF_prop.id is null --commented out for dev purposes.
 and active = 1
GO
