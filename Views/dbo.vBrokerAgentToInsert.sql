SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
Create View [dbo].[vBrokerAgentToInsert] as 
SELECT distinct ba.FirstName,
ba.LastName,
ba.email,
ba.phone
  FROM [AcquisitionCandidateAggregator].[dbo].ListingBrokerOfRecord lbor
  join [AcquisitionCandidateAggregator].[dbo].listing l on l.id = lbor.ListingId
  join [AcquisitionCandidateAggregator].[dbo].[BrokerAgent] ba on lbor.name = ba.FirstName+' '+ba.LastName
  left join [AcquisitionCandidateAggregator].[dbo].[BrokerAgentLicense] bal on ba.id = bal.BrokerAgentId
  join SalesForce_Reporting.dbo.vPropertyWithListingURL SF_property on SF_Property.Listing_Website_Id__c = l.ListingWebsiteId 
  left join SalesForce_Reporting.dbo.vBrokerAgent vba on vba.FirstName+' '+vba.LastName = ba.FirstName+' '+ba.LastName
  where vba.id is null
GO
