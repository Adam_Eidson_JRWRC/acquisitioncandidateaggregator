SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [STAGE].[vTheBoulderGroupUrl] as
select distinct idProperty,
pdf,
'https://bouldergroup.com/media/pdf/'+REPLACE(REPLACE(pdf,' ','%20'),',', '%2c') as PDF_Url
From AcquisitionCandidateAggregator.STAGE.TheBoulderGroup
where Latitude is  Null

GO
