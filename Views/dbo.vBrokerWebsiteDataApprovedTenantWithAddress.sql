SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[vBrokerWebsiteDataApprovedTenantWithAddress] as 
SELECT distinct [BrokerWebsiteId]
      
      ,[address]+' '+[city]+' '+[state]+' '+[PostalCode] as Address
	  ,[address] as Street
	  ,[city]
	  ,state
	  ,PostalCode
      
  FROM [AcquisitionCandidateAggregator].[dbo].[vBrokerWebsiteDataApprovedTenant]
  where address is not null
  
GO
