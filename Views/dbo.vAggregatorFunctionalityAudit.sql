SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
create view [dbo].[vAggregatorFunctionalityAudit] as 
SELECT top (100)  sys.fn_cdc_map_lsn_to_time(__$start_lsn) as startDate
      ,[__$end_lsn]
      ,[__$seqval]
      ,[__$operation]
      ,[__$update_mask]
      ,[id]
      ,[Name]
      ,[ListingURL]
      ,[ContractPrice]
      ,[SellerBrokerageId]
      ,[NOI]
      ,[SqFt]
      ,[YearBuilt]
      ,[Street]
      ,[City]
      ,[State]
      ,[Zip]
      ,[TypeOfOwnership]
      ,[yearRenovated]
      ,[CurrentTermExpirationDate]
      ,[NetLeaseType]
      ,[1MileRadiusPopulation]
      ,[MedianHouseholdIncome]
      ,[1MileRadiusVacantHousing]
      ,[1MileRadiusOwnerOccupiedHousing]
      ,[1MileRadiusRenterOccupiedHousing]
      ,[1MileRadiusAvgHHIncome]
      ,[3MileRadiusPopulation]
      ,[3MileRadiusVacantHousing]
      ,[3MileRadiusOwnerOccupiedHousing]
      ,[3MileRadiusRenterOccupiedHousing]
      ,[3MileRadiusAvgHHIncome]
      ,[5MileRadiusPopulation]
      ,[5MileRadiusVacantHousing]
      ,[5MileRadiusOwnerOccupiedHousing]
      ,[5MileRadiusRenterOccupiedHousing]
      ,[CrimeRateIndex]
      ,[ListingCreatedDate]
      ,[ListingWebsiteId]
      ,[Active]
      ,[Latitude]
      ,[Longitude]
      ,[__$command_id]
  FROM [AcquisitionCandidateAggregator].[cdc].[dbo_Listing_CT]
  --where ListingWebsiteId='5467905'--SellerBrokerageId = 3
  order by sys.fn_cdc_map_lsn_to_time(__$start_lsn) desc 
GO
