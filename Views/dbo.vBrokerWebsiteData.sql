SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE View [dbo].[vBrokerWebsiteData] as 
with cte_StanJohnsonCo as (
select null as BrokerWebsiteId
	  ,[listing_name] as listingName
	  ,[listing_name] as Tenant
      ,[Price] as ListingPrice
      ,CASE WHEN [Cap_RateEquity] like '%Equity%' 
	  then REPLACE([Cap_RateEquity], ' Equity', '') 
	  else null 
	  end
	  as Equity,
	  CASE WHEN [Cap_RateEquity] not like '%Equity%' 
	  then REPLACE([Cap_RateEquity] , '%','')
	  else null
	  end as CapRate
      ,[details]
      ,[View Details] as PropertyURL
	  ,'StanJohnsonCo' as BrokerWebsiteSource
	  ,null as [city]
	  ,null as [state]
	  ,null as [address]
	  ,null as [PostalCode]
	  ,null as [noi]
	  	  ,null as [Price/Space],
null as [Number of Units],
null as [Price/Bed],
null as [Price/Room],
null as [Price/Gross SF],
null as [Gross SF],
null as [Years Remaining On Lease],
null as [Lot Size],
null as [Year Built],
GETDATE() as ListingDatetime
  FROM [AcquisitionCandidateAggregator].[STAGE].[StanJohnsonCo]
  ), cte_TheBoulderGroup as (

  select  [idProperty] as BrokerWebsiteId
      ,[idActive]
      ,[timestamp] as ListingDatetime
      ,[idPriority]
      ,[title] as listingName
	  ,[title] as Tenant
      ,[city]
      ,[state]
	  ,null as [address]
	  ,null as [PostalCode]
      ,cast([price] as varchar(20))as ListingPrice
      ,[cap] as CapRate
      ,[capLabel] 
      ,[term]
      ,[termLabel]
      ,'https://www.bouldergroup.com/media/pdf/'+[pdf] as PDF
	  ,'https://www.bouldergroup.com/media/pdf/'+[pdf] as [PropertyUrl]
      ,[image]
	  ,'TheBoulderGroup' as BrokerWebsiteSource
	  ,null as [noi]
	  	  ,null as [Price/Space],
null as [Number of Units],
null as [Price/Bed],
null as [Price/Room],
null as [Price/Gross SF],
null as [Gross SF],
null as [Years Remaining On Lease],
null as [Lot Size],
null as [Year Built]
  FROM [AcquisitionCandidateAggregator].[STAGE].[TheBoulderGroup]
  ), cte_MarcusMillichap as (
  select *
  From (select  [PropertyId] as BrokerWebsiteId
      ,[PropertyName]  as listingName
	  ,[PropertyName]  as Tenant
      ,[PropertyTypeId]
      ,[PropertyType]
      ,[PropertySubTypeId]
      ,[PropertySubType]
      ,[FirstImageUrl]
      ,[SecondImageUrl]
      ,TRIM([Address1] +' '+[Address2]) as [address]
      ,[City]
      ,[StateProvince] as [state]
      ,[StateProvinceName]
      ,[PostalCode] 
      ,[DynamicDataText]
      ,[DynamicDataValue]
      ,cast([CapRate] as float) as [CapRate]
      ,[ListingPrice]
      ,[PropertyUrl]
      ,[Tile]
	  ,'MarcusMillichap' as BrokerWebsiteSource
	  ,null as [noi]
	  ,GETDATE() as ListingDatetime
  FROM [AcquisitionCandidateAggregator].[STAGE].[MarcusMillichap] )  t
PIVOT (
max(DynamicDataValue)
for [DynamicDataText] in (
[Price/Space],
[Number of Units],
[Price/Bed],
[Price/Room],
[Price/Gross SF],
[Gross SF],
[Years Remaining On Lease],
[Lot Size],
[Year Built])
)
as pvtTable
  ), cte_Matthews as (
  select [id] as BrokerWebsiteId
      ,[latitude]
      ,[longitude]
      ,[type]
      ,[title] as listingName
	  ,[title] as Tenant
      ,[status]
      ,[address]
      ,[city]
      ,[state]
      ,cast([zip] as varchar(10)) as [PostalCode]
      ,[year_built]
      ,[price] as ListingPrice
      ,[noi]
      ,[leasable_area]
      ,[price_per_sq]
      ,cast(REPLACE([cap_rate],'%','') as float) as CapRate
      ,[units]
      ,[link] as PropertyURL
      ,[images]
      ,[space_use]
	  ,'Matthews' as BrokerWebsiteSource
	  ,null as [Price/Space],
null as [Number of Units],
null as [Price/Bed],
null as [Price/Room],
null as [Price/Gross SF],
null as [Gross SF],
null as [Years Remaining On Lease],
null as [Lot Size],
null as [Year Built],
GETDATE() as ListingDatetime
  FROM [AcquisitionCandidateAggregator].[STAGE].[Matthews]
  )
  select BrokerWebsiteId,
  listingName,
  ListingPrice,
  case when CapRate is null
  then cast(-100 as float)
  else REPLACE(CapRate , '%','')
  end as CapRate,
  case when [noi] = ''
  then -100
  when [noi] is null
  then -100
else  cast(REPLACE(REPLACE(REPLACE([noi], '$',''),',',''),'To Be Negotiated: ','') as int)
  end 
  as [noi],
  [Price/Space],
 [Number of Units],
[Price/Bed],
[Price/Room],
[Price/Gross SF],
[Gross SF],
cast(replace([Years Remaining On Lease],',','') as float) as [Years Remaining On Lease],
[Lot Size],
[Year Built],
  [address],
  city,
  state,
  [PostalCode],
  PropertyURL,
  BrokerWebsiteSource,
  Tenant
  from cte_Matthews
  union 
  select BrokerWebsiteId,
  listingName,
  ListingPrice,
  case when CapRate is null
  then -100
  else CapRate
  end as CapRate,
  case when [noi] = ''
  then  cast(-100 as float)
  when [noi] is null
  then -100
  else  cast(REPLACE(REPLACE(REPLACE([noi], '$',''),',',''),'To Be Negotiated: ','') as int)
  end 
  as [noi],
  case when [Price/Space] = ''
  then null
  else [Price/Space]
  end as [Price/Space],
 case when [Number of Units] = ''
  then null
  else [Number of Units]
  end as [Number of Units],
case when [Price/Bed] = ''
  then null
  else [Price/Bed]
  end as [Price/Bed],
case when [Price/Room] = ''
  then null
  else [Price/Room]
  end as [Price/Room],
case when [Price/Gross SF] = ''
  then null
  else [Price/Gross SF]
  end as [Price/Gross SF],
case when [Gross SF] = ''
  then null
  else [Gross SF]
  end as [Gross SF],
case when [Years Remaining On Lease] = ''
  then null
  else cast(replace([Years Remaining On Lease],',','') as float)
  end as [Years Remaining On Lease],
case when [Lot Size] = ''
  then null
  else [Lot Size]
  end as [Lot Size],
case when [Year Built] = ''
  then null
  else REPLACE([Year Built], ',','')
  end as [Year Built],
  [address],
  city,
  state,
  [PostalCode],
  PropertyURL,
  BrokerWebsiteSource,
  Tenant
  From cte_MarcusMillichap
  union 
  select BrokerWebsiteId,
  listingName,
  ListingPrice,
  case when CapRate is null
  then  cast(-100 as float)
  else CapRate
  end as CapRate,
  case when [noi] = ''
  then -100
  when [noi] is null
  then -100
  else  cast(REPLACE(REPLACE(REPLACE([noi], '$',''),',',''),'To Be Negotiated: ','') as int)
  end 
  as [noi],
  [Price/Space],
 [Number of Units],
[Price/Bed],
[Price/Room],
[Price/Gross SF],
[Gross SF],
cast(replace([Years Remaining On Lease],',','') as float) as [Years Remaining On Lease],
[Lot Size],
[Year Built],
  [address],
  city,
  state,
  [PostalCode],
  PropertyURL,
  BrokerWebsiteSource,
  Tenant
  From cte_TheBoulderGroup
  union 
  select BrokerWebsiteId,
  listingName,
  ListingPrice,
  case when CapRate is null
  then  cast(-100 as float)
  else CapRate
  end as CapRate,
  case when [noi] = ''
  then -100
   when [noi] is null
  then -100
  else  cast(REPLACE(REPLACE(REPLACE([noi], '$',''),',',''),'To Be Negotiated: ','') as int)
  end 
  as [noi],
 	  [Price/Space],
 [Number of Units],
[Price/Bed],
[Price/Room],
[Price/Gross SF],
[Gross SF],
cast(replace([Years Remaining On Lease],',','') as float) as [Years Remaining On Lease],
[Lot Size],
[Year Built],
  [address],
  city,
  state,
  [PostalCode],
  PropertyURL,
  BrokerWebsiteSource,
  Tenant
  From cte_StanJohnsonCo
GO
