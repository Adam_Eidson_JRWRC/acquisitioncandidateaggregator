SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











create View [STAGE].[vBrokerWebsiteDataApprovedTenant] as 
with cte_StanJohnsonCo as (
select LocalListingId as BrokerWebsiteId
	  ,[listing_name] as listingName
	  ,REPLACE([listing_name],'"','') as Tenant
      ,REPLACE([Price],'"','') as ListingPrice
      ,CASE WHEN [Cap_RateEquity] like '%Equity%' 
	  then REPLACE(REPLACE([Cap_RateEquity], ' Equity', ''),'"','') 
	  else null 
	  end
	  as Equity,
	  CASE WHEN [Cap_RateEquity] not like '%Equity%' 
	  then REPLACE(REPLACE(REPLACE([Cap_RateEquity] , '%',''),'"',''), 'None',0)
	  else null
	  end as CapRate
      ,[details]
      ,[ViewDetails] as PropertyURL
	  ,'Stan Johnson Company' as BrokerWebsiteSource
	  ,null as [city]
	  ,null as [state]
	  ,null as [address]
	  ,null as [PostalCode]
	  ,null as [noi]
	  	  ,null as [Price/Space],
null as [Number of Units],
null as [Price/Bed],
null as [Price/Room],
null as [Price/Gross SF],
null as [Gross SF],
null as [Years Remaining On Lease],
null as [Lot Size],
null as [Year Built],

null as LeaseType,
null as PropertyType,
null  as PropertySubType,
null as [Ownership],
null as YearRenovated,
 null as LeaseOptions,
 null as TenantCredit,
 null as LeaseExpiration,
 null as [BrokerOfRecordName],
null as ListingDatetime
  FROM [AcquisitionCandidateAggregator].[STAGE].[StanJohnsonCo]
  ), cte_TheBoulderGroup as (

  select  [idProperty] as BrokerWebsiteId
      ,[idActive]
      ,[timestamp] as ListingDatetime
      ,[idPriority]
      ,[title] as listingName
	  ,[title] as Tenant
      ,[city]
      ,[state]
	  ,null as [address]
	  ,null as [PostalCode]
      ,cast([price] as varchar(20))as ListingPrice
      ,[cap] as CapRate
      ,[capLabel] 
      ,[term]
      ,[termLabel]
      ,'https://www.bouldergroup.com/media/pdf/'+[pdf] as PDF
	  ,'https://www.bouldergroup.com/media/pdf/'+[pdf] as [PropertyUrl]
      ,[image]
	  ,'The Boulder Group' as BrokerWebsiteSource
	  ,null as [noi]
	  	  ,null as [Price/Space],
null as [Number of Units],
null as [Price/Bed],
null as [Price/Room],
null as [Price/Gross SF],
null as [Gross SF],
null as [Years Remaining On Lease],
null as [Lot Size],
null as [Year Built],

null as LeaseType,
null  as PropertySubType,
null as PropertyType,
null as [Ownership],
null as YearRenovated,
 null as LeaseOptions,
 null as TenantCredit,
 null as LeaseExpiration,
 null as [BrokerOfRecordName]
  FROM [AcquisitionCandidateAggregator].[STAGE].[TheBoulderGroup]
  ), cte_MarcusMillichap as (
  select *
  From (select  [PropertyId] as BrokerWebsiteId
      ,[PropertyName]  as listingName
	  ,[PropertyName]  as Tenant
      ,[PropertyTypeId]
      ,[PropertyType]
      ,[PropertySubTypeId]
      ,[PropertySubType]
      ,[FirstImageUrl]
      ,[SecondImageUrl]
      ,TRIM([Address1] +' '+isnull([Address2],'')) as [address]
      ,[City]
      ,[StateProvince] as [state]
      ,[StateProvinceName]
      ,[PostalCode] 
      ,[DynamicDataText]
      ,cast([DynamicDataValue] as varchar(50)) as [DynamicDataValue]
      ,cast([CapRate] as float) as [CapRate]
      ,[ListingPrice]
      ,[PropertyUrl]
      ,[Tile]
	  ,'Marcus & Millichap' as BrokerWebsiteSource
	  ,null as [noi]
	  
,null as LeaseType,
null as [Ownership],
null as YearRenovated,
 null as LeaseOptions,
 null as TenantCredit,
 null as LeaseExpiration,
 null as [BrokerOfRecordName],
	  null as ListingDatetime
  FROM [AcquisitionCandidateAggregator].[STAGE].[MarcusMillichap] )  t
PIVOT (
max(DynamicDataValue)
for [DynamicDataText] in (
[Price/Space],
[Number of Units],
[Price/Bed],
[Price/Room],
[Price/Gross SF],
[Gross SF],
[Years Remaining On Lease],
[Lot Size],
[Year Built])
)
as pvtTable
  ), cte_Matthews as (
  select [id] as BrokerWebsiteId
      ,[latitude]
      ,[longitude]
      ,[type] as PropertyType
      ,[title] as listingName
	  ,[title] as Tenant
      ,[status]
      ,[address]
      ,[city]
      ,[state]
      ,cast([zip] as varchar(10)) as [PostalCode]
      ,[year_built]
      ,[price] as ListingPrice
      ,[noi]
      ,[leasable_area]
      ,[price_per_sq]
      ,cast(REPLACE([cap_rate],'%','') as float) as CapRate
      ,[units]
      ,[link] as PropertyURL
      ,[space_use]
	  ,'Matthews Real Estate Investment Services' as BrokerWebsiteSource
	  ,null as [Price/Space],
null as [Number of Units],
null as [Price/Bed],
null as [Price/Room],
null as [Price/Gross SF],
null as [Gross SF],
null as [Years Remaining On Lease],
null as [Lot Size],
year_built as [Year Built],
null as LeaseType,
null  as PropertySubType,
null as [Ownership],
null as YearRenovated,
 null as LeaseOptions,
 null as TenantCredit,
 null as LeaseExpiration,
 null as [BrokerOfRecordName],
null as ListingDatetime
  FROM [AcquisitionCandidateAggregator].[STAGE].[Matthews]
  ), cte_CREXi as (
  Select cbase.id as BrokerWebsiteId,
cBase.Title as listingName,
tenant.TenantName as Tenant,
case when asset.[Asking Price] = ''
then null
else asset.[Asking Price] 
end as ListingPrice,
asset.[Cap Rate] as CapRate,
 cBase.Url as PropertyURL,
 'CREXi' as BrokerWebsiteSource,
 City,
 State,
 Street,
 [Lot Size (acres)] as [Lot Size],
 PostalCode,
 Noi as noi,
 asset.[Square Footage] as [Gross SF],
 asset.[Lease Type] as LeaseType,
 asset.[Property Type] as PropertyType,
 asset.[Subtype] as PropertySubType,
 asset.Ownership as [Ownership],
 asset.[Year Renovated] as YearRenovated,
 asset.[Lease Options] as LeaseOptions,
 asset.[Tenant Credit] as TenantCredit,
 asset.[Lease Expiration] as LeaseExpiration,
 case when [Remaining Term] = ''
then null 
else cast(REPLACE(REPLACE(replace([Remaining Term],',',''),' years',''),' year','')  as float) 
end as [Years Remaining On Lease],
 asset.[Lease Term] as LeaseTerm,
 asset.[Year Built] ,
 cBase.[BrokerOfRecordName],
 cBase.CreatedOn as ListingDatetime
From Crexi cBase
left join CREXiAddressDetail addr on addr.Id = cBase.Id
left join CREXiAssetDetail asset on asset.Id = cBase.Id
left join CREXiTenantDetail tenant on tenant.Id = cBase.Id
  ), cte_Fortis as (
  
  Select *
  --t.[BrokerWebsiteId]
  --    ,t.[Gross SF]
  --    ,t.[ListingName]
  --    ,t.[Street]
  --    ,t.[City]
  --    ,t.[State]
  --    ,t.[PostalCode]
  --    ,t.PropertySubType
  --    ,t.[Latitude]
  --    ,t.[Longitude]
  --    ,t.[ListingURL]
  --,t.[BrokerOfRecordName],
  --t.BrokerAgent_Email,
  --t.BrokerAgentPhone,
  --CapRate,
  --Price as ListingPrice
  From (
  select fn.[BrokerWebsiteId]
      ,[Size] as [Gross SF]
      ,[ListingName]
      ,[Street]
      ,[City]
      ,[State]
      ,[PostalCode]
      ,[SubType] as PropertySubType
      ,[Latitude]
      ,[Longitude]
      ,[ListingURL] as PropertyURL
  ,bl.name as [BrokerOfRecordName],
  bl.email as BrokerAgent_Email,
  bl.phone as BrokerAgentPhone,
  la.Value,
  la.attribute
  ,[ListingName] as Tenant
  ,null as [noi]
,null as LeaseType,
null as [Ownership],
null as YearRenovated,
 null as LeaseOptions,
 null as TenantCredit,
 null as LeaseExpiration,
	  null as ListingDatetime,
	  null as [Years Remaining On Lease],
	  null as [Lot Size],
	  null as [Year Built],
	  null as [PropertyType],
  'Fortis Net Lease' as BrokerWebsiteSource
  from [AcquisitionCandidateAggregator].[STAGE].FortisNet fn
  left join [AcquisitionCandidateAggregator].[STAGE].FortisNetBrokerList bl on bl.BrokerWebsiteId = fn.BrokerWebsiteId
  left join [AcquisitionCandidateAggregator].[STAGE].FortisNetListingAttribute la on la.BrokerWebsiteId = fn.BrokerWebsiteId
 ) t
  PIVOT (
max(Value) 
for [Attribute] in ([ListingPrice], [CapRate]
)
)
as pvtTable

)
  -------------------------------------------------
  -------------------------------------------------
  --CTE Table Finish
  -------------------------------------------------
  -------------------------------------------------
  
  -------------------------------------------------
  -------------------------------------------------
  --UNION Begin
  -------------------------------------------------
  -------------------------------------------------

  select x.*
  from(
 select Name 
 from ApprovedTenant
) as v (Name)

CROSS APPLY 
(
  select BrokerWebsiteId,
  listingName,
  ListingPrice,
REPLACE(CapRate , '%','') as CapRate,
 cast(REPLACE(REPLACE(REPLACE([noi], '$',''),',',''),'To Be Negotiated: ','') as float ) as [noi],
[Gross SF],
cast(replace([Years Remaining On Lease],',','') as float) as [Years Remaining On Lease],
[Lot Size],
[Year Built],
  [address],
  city,
  state,
  [PostalCode],
  PropertyURL,
  BrokerWebsiteSource,
  Tenant,
  PropertyType,
 PropertySubType,
 [Ownership],
 YearRenovated,
 LeaseOptions,
 TenantCredit,
 LeaseExpiration,
 LeaseType,
 [BrokerOfRecordName],
 ListingDatetime
  from cte_Matthews
  where Tenant like v.Name) as x
  union 
  select x.*
  from(
 select Name 
 from ApprovedTenant
) as v (Name)

CROSS APPLY 
(
  select BrokerWebsiteId,
  listingName,
  ListingPrice,
CapRate,
cast(REPLACE(REPLACE(REPLACE([noi], '$',''),',',''),'To Be Negotiated: ','') as float)  as [noi],
case when [Gross SF] = ''
  then null
  else [Gross SF]
  end as [Gross SF],
case when [Years Remaining On Lease] = ''
  then null
  else cast(replace([Years Remaining On Lease],',','') as float)
  end as [Years Remaining On Lease],
case when [Lot Size] = ''
  then null
  else [Lot Size]
  end as [Lot Size],
case when [Year Built] = ''
  then null
  else REPLACE([Year Built], ',','')
  end as [Year Built],
  [address],
  city,
  state,
  [PostalCode],
  PropertyURL,
  BrokerWebsiteSource,
  Tenant,
  PropertyType,
 PropertySubType,
 [Ownership],
 YearRenovated,
 LeaseOptions,
 TenantCredit,
 LeaseExpiration,
 LeaseType,
 [BrokerOfRecordName],
 ListingDatetime
 
  From cte_MarcusMillichap
  where Tenant like v.Name) as x
  union 
  select x.*
  from(
 select Name 
 from ApprovedTenant
) as v (Name)

CROSS APPLY 
(
  select BrokerWebsiteId,
  listingName,
  ListingPrice,
CapRate,
cast(REPLACE(REPLACE(REPLACE([noi], '$',''),',',''),'To Be Negotiated: ','') as float)  as [noi],
[Gross SF],
cast(replace([Years Remaining On Lease],',','') as float) as [Years Remaining On Lease],
[Lot Size],
[Year Built],
  [address],
  city,
  state,
  [PostalCode],
  PropertyURL,
  BrokerWebsiteSource,
  Tenant,
  PropertyType,
 PropertySubType,
 [Ownership],
 YearRenovated,
 LeaseOptions,
 TenantCredit,
 LeaseExpiration,
 LeaseType,
 [BrokerOfRecordName],
 ListingDatetime
 
  From cte_TheBoulderGroup
  where Tenant like v.Name) as x
  union 
  select x.*
  from(
 select Name 
 from ApprovedTenant
) as v (Name)

CROSS APPLY 
(
  select BrokerWebsiteId,
  listingName,
  ListingPrice,
	CapRate,
  cast(REPLACE(REPLACE(REPLACE([noi], '$',''),',',''),'To Be Negotiated: ','') as float)  as [noi],
[Gross SF],
cast(replace([Years Remaining On Lease],',','') as float) as [Years Remaining On Lease],
[Lot Size],
[Year Built],
  [address],
  city,
  state,
  [PostalCode],
  PropertyURL,
  BrokerWebsiteSource,
  Tenant,
  PropertyType,
 PropertySubType,
 [Ownership],
 YearRenovated,
 LeaseOptions,
 TenantCredit,
 LeaseExpiration,
 LeaseType,
 [BrokerOfRecordName],
 ListingDatetime
 
  From cte_StanJohnsonCo
  where Tenant like v.Name) as x
union
select x.*
  from(
 select Name 
 from ApprovedTenant
) as v (Name)

CROSS APPLY 
(
  select BrokerWebsiteId,
  listingName,
  ListingPrice,
  case when CapRate is null
  then cast(-100 as float)
  else cast(REPLACE(CapRate,'%','') as float)
  end as CapRate,
cast(REPLACE(REPLACE(REPLACE([noi], '$',''),',',''),'To Be Negotiated: ','') as float)  as [noi],
[Gross SF],
cast(replace([Years Remaining On Lease],',','') as float) as [Years Remaining On Lease],
cast([Lot Size] as float) as [Lot Size],
[Year Built],
  [street] as address,
  city,
  state,
  [PostalCode],
  PropertyURL,
  BrokerWebsiteSource,
  Tenant,
  PropertyType,
 PropertySubType,
 [Ownership],
 YearRenovated,
 LeaseOptions,
 TenantCredit,
 LeaseExpiration,
 LeaseType,
 [BrokerOfRecordName],
 ListingDatetime
 
  from cte_CREXi
  where Tenant like v.Name) as x
 UNION
  select x.*
  from(
 select Name 
 from ApprovedTenant
) as v (Name)

CROSS APPLY 
( select BrokerWebsiteId,
  listingName,
  ListingPrice,
  case when CapRate is null
  then cast(-100 as float)
  else cast(REPLACE(CapRate,'%','') as float)
  end as CapRate,
cast(REPLACE(REPLACE(REPLACE([noi], '$',''),',',''),'To Be Negotiated: ','') as float)  as [noi],
[Gross SF],
cast(replace([Years Remaining On Lease],',','') as float) as [Years Remaining On Lease],
cast([Lot Size] as float) as [Lot Size],
[Year Built],
  [street] as address,
  city,
  state,
  [PostalCode],
  PropertyURL,
  BrokerWebsiteSource,
  Tenant,
  PropertyType,
 PropertySubType,
 [Ownership],
 YearRenovated,
 LeaseOptions,
 TenantCredit,
 LeaseExpiration,
 LeaseType,
 [BrokerOfRecordName],
 ListingDatetime
 
  from cte_Fortis
  where Tenant like v.Name) as x
GO
