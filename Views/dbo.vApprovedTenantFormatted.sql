SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[vApprovedTenantFormatted] as
SELECT distinct REPLACE([Name],'%','') as TenantName
  FROM [AcquisitionCandidateAggregator].[dbo].[ApprovedTenant]
GO
