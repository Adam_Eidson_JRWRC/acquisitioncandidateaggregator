SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [dbo].[vBrokerAgentDiscovery] as 
 with cte_approvedTenant as (
 select distinct x.*
  from(
 select Name,
 NormalizedName
 from ApprovedTenant
) as v (Name, NormalizedName)

CROSS APPLY 
( SELECT distinct l.*,
sb.name as sellerBrokerageName,
lbor.Name as [BrokerOfRecordName],
v.NormalizedName as tenantName
  FROM [AcquisitionCandidateAggregator].[dbo].[Listing] l
  left join [AcquisitionCandidateAggregator].[dbo].SellerBrokerage sb on sb.id = l.sellerbrokerageid
  left join [AcquisitionCandidateAggregator].[dbo].ListingBrokerOfRecord lbor on lbor.ListingId = l.id
   where l.name  like v.Name) as x
   )
   
SELECT distinct l.ListingWebsiteId, 
lbor.name as BrokerName, 
l.sellerBrokerageName,
license, 
email,
phone,
l.ListingURL,
l.name,
l.tenantName
  FROM cte_approvedTenant l
  left join [AcquisitionCandidateAggregator].[dbo].[ListingBrokerOfRecord] lbor on l.id = lbor.ListingId
GO
