SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




























CREATE View [dbo].[vBrokerWebsiteDataPendingApprovalTenant] as 

 select distinct x.*
  from(
 select Name 
 from PendingApproval
 --where active = 1
) as v (Name)

CROSS APPLY 
( SELECT distinct l.*,
sb.name as sellerBrokerageName,
lbor.Name as [BrokerOfRecordName],
lbor.email as BrokerOfRecordEmail,
lbor.License as BrokerOfRecordLicense,
lbor.phone as BrokerOfRecordPhone
  FROM [AcquisitionCandidateAggregator].[dbo].[Listing] l
  left join [AcquisitionCandidateAggregator].[dbo].SellerBrokerage sb on sb.id = l.sellerbrokerageid
  left join [AcquisitionCandidateAggregator].[dbo].ListingBrokerOfRecord lbor on lbor.ListingId = l.id
   where l.name  like v.Name) as x

GO
