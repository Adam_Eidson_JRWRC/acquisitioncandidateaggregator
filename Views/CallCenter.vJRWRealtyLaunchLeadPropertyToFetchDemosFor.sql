SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE VIEW [CallCenter].[vJRWRealtyLaunchLeadPropertyToFetchDemosFor] as 
with cte_AddressPrep as (
select sf.id, 
case when Street__c like '%ste %'
then 
SUBSTRING(Street__c,0,CHARINDEX('Ste ',Street__c)) 
when Street__c like '%suite%'
then 
SUBSTRING(Street__c,0,CHARINDEX('suite',Street__c)) 
when Street__c like '%-%'
then SUBSTRING(Street__c,CharIndex('-',Street__c)+1,75)
else Street__c
end as Street__c,
City__c,
State__c,
Zip__c,
sf.CreatedDate
From openquery(SALESFORCEDEVART,'select id,CreatedDate, Street__c, City__c, State__c, Zip__c from Property__c ' )sf
--openquery(SALESFORCEDEVART,'Select Id, AccountId,MailingLatitude, MailingLongitude, MailingStreet , MailingCity, MailingState, MailingPostalCode from Contact where AccountId = ''0010b00002dgnqJ''') sf
left join AcquisitionCandidateAggregator.CallCenter.Demographic d on d.BrokerWebsiteId = sf.id
where d.id is null
and convert(date,sf.CreatedDate,121) > '10/20/2020'

)

select distinct id as ListingId,
'SalesForce' as BrokerWebsiteSource,
      Street__c+' '+City__c+' '+State__c+' '+Zip__c as Address,
	   Street__c as Street,
	   City__c as City,
	   State__c as State,
	   Zip__c as PostalCode

from cte_AddressPrep
where Street__c is not null
and City__c is not null
and State__c is not null
and Zip__c is not null

--JRW Realty Launch Leads

--JRW Realty AnswerPro
GO
