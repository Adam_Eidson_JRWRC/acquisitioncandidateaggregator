SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE View [STAGE].[vJLLListingWithoutTenantAssociation] as
Select  distinct  l.Listingid as ListingId,
Latitude,
Longitude
from STAGE.JLL L
left join STAGE.JLLListingTenant lt on lt.ListingId = l.Listingid
where lt.TenantName is null
GO
