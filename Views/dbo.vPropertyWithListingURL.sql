SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vPropertyWithListingURL] as 
Select *
From openquery(SALESFORCEDEVARTFULLCOPY, 'Select Id, Listing_URL__c from Property__c where Listing_URL__c is not null')
GO
